# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: byron.pdf chapter01.pdf chapter02.pdf chapter03.pdf chapter04.pdf all clean

# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
#all: byron.pdf chapter01.pdf chapter02.pdf chapter03.pdf chapter04.pdf
all: chapter04.pdf

# CUSTOM BUILD RULES

# In case you didn't know, '$@' is a variable holding the name of the target,
# and '$<' is a variable holding the (first) dependency of a rule.
# "raw2tex" and "dat2tex" are just placeholders for whatever custom steps
# you might have.

%.tex: %.raw
	./raw2tex $< > $@

%.tex: %.dat
	./dat2tex $< > $@

# MAIN LATEXMK RULE

# -pdf tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make tells latexmk to call make for generating missing files.

# -interaction=nonstopmode keeps the pdflatex backend from stopping at a
# missing file reference and interactively asking you for an alternative.

byron.pdf:
	latexmk -bibtex -xelatex -use-make byron.tex

chapter01.pdf:
	latexmk -bibtex -xelatex -use-make chapter01.tex

chapter02.pdf:
	latexmk -bibtex -xelatex -use-make chapter02.tex

chapter03.pdf:
	latexmk -bibtex -xelatex -use-make chapter03.tex

chapter04.pdf:
	latexmk -bibtex -xelatex -use-make chapter04.tex

clean:
	latexmk -CA
