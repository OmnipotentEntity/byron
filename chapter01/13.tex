\prompt{The electromagnetic field tensor in free space. Problems 11 and 12 have
laid the groundwork for the four-dimensional theory of the electromagnetic field
that will unfold as this problem is done.  We shall use Gaussian units.  To
begin, if we identify the vector $\vec{H}_k$ of Problem 11 with the magnetic
field, then problem 12 relates $\vec{H}_k$ to a second-rank antisymmetric
tensor, $\mat{F}_{ij}$.}

\prompt{(a) Prove}

\[
  \mat{F}_{ij} = \pdv{A_j}{x_i} - \pdv{A_i}{x_j} = \pd{i} A_j - \pd{j} A_i
\]

\prompt{where $\curl \vec{A} = \vec{H}$.}

The proof is performed easily through the definition of $\mat{F}$ and the curl.

\begin{align*}
  H_k &= {\qty(\curl \vec{A})}_k \\
  &= \qty( \pdv{A_j}{x_i} - \pdv{A_i}{x_j}) \\
  F_{ij} &= \llevic{ijk} H_k \\
  &= \llevic{ijk} \qty( \pdv{A_j}{x_i} - \pdv{A_i}{x_j}) \\
  &= \qty( \pdv{A_j}{x_i} - \pdv{A_i}{x_j})
\end{align*}

\prompt{(b) Show directly from (a) $\mat{F}_{ij}$ is anti-symmetric.}

\begin{align*}
  F_{ij} &= (\pd{i} A_j - \pd{j} A_i) \\
  -F_{ij} &= (\pd{j} A_i - \pd{i} A_j) \\
  &= F_{ji}
\end{align*}

\prompt{So far, $\mat{F}_{ij}$ is three-dimensional and has $3^2 = 9$
components.  We now extend it to four dimensions.}

\prompt{(c) Make the identifications $x_4 = ict$ and $A_4 = i\varphi$, where $i
= \sqrt{-1}$ and $\varphi$ is the electrostatic potential.  Prove that $F_{4j} =
-F_{j4} = iE_j$ ($j = 1, 2, 3$), using $\mat{F}_{ij}$ as expressed in (a) and in
problem 11(b).}

\begin{align*}
  x_4 &= ict \\
  A_4 &= i\varphi \\
  F_{4j} &= (\pd{x_4} A_j - \pd{j} A_4) \\
  \pdv{x_4} A_j - \pdv{x_j} A_4 &= \pdv{(ict)} A_j - \pdv{x_j} (i \varphi) \\
  &= - \frac{i}{c} \pdv{A_j}{t} - i{(\grad \varphi)}_j \\
\end{align*}

From problem 11 we know that:

\[
  \vec{E} = - \frac1{c} \pdv{\vec{A}}{t} - \grad \varphi
\]

So,

\[
  F_{4j} = iE_j
\]

And by the same reasoning as part (b), F is still antisymmetric so $-F_{j4} =
iE_j$.

\prompt{(d) Verify that $F_{ii} = 0$ ($i=1,2,3,4$).}

\begin{align*}
  F_{ii} &= \pd{i} A_i - \pd{i} A_i \\
  &= 0 \\
\end{align*}

\prompt{Thus $\mat{F}_{ij}$ is a four-dimensional antisymmetric second-rank
tensor with $4^2 = 16$ components.  The four diagonal elements are zero.  Only
six of the remaining 12 elements are independent, expressing the fact that
$\vec{E}$ and $\vec{H}$ have a total of six components.}

\prompt{(e) Write out $\mat{F}_{ij}$ as a 4 by 4 array explicitly in terms of
the components of $\vec{E}$ and $\vec{H}$.}

The values for the 4th row and column have been computed in (c).  The upper
left $3 \times 3$ can be obtained using just the definition of the curl.  For
reference:

\[
  \mat{F}_{ij} = \begin{bmatrix}
    0 & H_z & -H_y & -iE_x \\
    -H_z & 0 & H_x & -iE_y \\
    H_y & -H_x & 0 & -iE_z \\
    iE_x & iE_y & iE_z & 0 \\
  \end{bmatrix}
\]

\prompt{All four of Maxwell's equations can be expressed in two equations
written in terms of the electromagnetic field tensor:}

\begin{align}\leqn{binachi}
  \pd{l} F_{ik} + \pd{i} F_{kl} + \pd{k} F_{li} &= 0 \\
  \pd{k} F_{ik} &= \frac{4\pi}{c} J_i
\end{align}

\prompt{where $J_i = {(\vec{J})}_i$ for $i = 1,2,3$ and $J_4 = ic\rho$ ($\rho =$
electric-charge density). In these two equations $i$, $k$, and $l$ extend from 1
to 4.  Prove that these equations subsume Maxwell's equations, and give nothing
more. (This will involve considering different combinations of subscripts,
etc.)}

(Equation~\reqn{binachi} is known as the Binachi identity.)

\medskip

(i) Gauss's Law: $(\div \vec{E} = 4 \pi \rho)$

\begin{align*}
  \pd{k} F_{ik} &= \frac{4\pi}{c} J_i \\
  i &= 4 \\
  \pd{x} (i E_x) + \pd{y} (i E_y) + \pd{z}(i E_z) &= \frac{4\pi}{c} ic\rho \\
  \div \vec{E} &= 4 \pi \rho
\end{align*}

\medskip 

(ii) Gauss's Law for Magnetism: $(\div \vec{H} = 0)$

\[
  \div \vec{H} = \div (\curl \vec{A}) = 0
\]

\medskip 

(iii) Maxwell-Faraday Equation: $\qty(\curl \vec{E} = -\frac1{c} \pdv{t} \vec{H})$

Using the Binachi identity, with $l=2, i=4, k=3$ 

\begin{align*}
  \pd{2} F_{43} + \pd{x_4} F_{32} + \pd{3} F_{24} &= 0 \\
  \pdv{y} (i E_z) + \frac1{ic} \pdv{t} (-H_x) + \pdv{z}(-i E_y) &= 0 \\
  {(\curl \vec{E})}_x = -\frac1{c} \pdv{H_x}{t} \\
\end{align*}

Similarly, we can make the identities for $y$ and $z$ by choosing $l$ and $k$ to
be $1,3$ and $1,2$ respectively, giving:

\[
  \curl \vec{E} = -\frac1{c} \pdv{\vec{H}}{t}
\]

\medskip 

(iv) Ampere's Law: $\qty(\curl \vec{H} = \frac1{c} \qty(4 \pi \vec{J} + \pdv{\vec{E}}{t}))$

\begin{align*}
  \pd{k} F_{ik} &= \frac{4\pi}{c} J_i \\
  i &= 1 \\
  \pd{x} (0) + \pd{y}(H_z) + \pd{z}(-H_y) + \pd{(ict)}(-iE_x) &= \frac{4\pi}{c} J_x \\
  (\curl \vec{H})_x &= \frac1{c} \pdv{E_x}{t} + \frac{4\pi}{c} J_x \\
\end{align*}

Similar results can be had from selecting $i=2$ and $i=3$ giving:

\[
  \curl \vec{H} = \frac1{c} \qty( \pdv{\vec{E}}{t} + \frac{4\pi}{c} \vec{J})
\]

No other equations are possible from the tensor and the two identities from
before, because they have been used in every possible combintation.

\medskip

\prompt{(g) Derive the four-dimensional form of the equation of conservation of
charge direction from Maxwell's equation (ii) in part (f). Interpret it in
four-dimensional form.}

Given that $J_4 = ic\rho$, it's possible to derive the right hand side of the
continuity equation by simply taking the divergence of the four-vector
$\vec{J}$. (Yielding $\pdv*{\rho}{t} + \div \vec{J}$).

The left hand side of the equation should therefore equal zero when its
divergence is taken.

\begin{align*}
  \pd{i} \pd{k} F_{ik} = \,
    &\pd{x} \pd{y} H_z + \pd{x} \pd{z} (-H_y) + \pd{x} \pd{ict} (-iE_x) \\
    &+ \pd{y} \pd{x} (-H_z) + \pd{y} \pd{z} H_x + \pd{y} \pd{ict} (-iE_y) \\
    &+ \pd{z} \pd{x} H_y + \pd{z} \pd{y} (-H_x) + \pd{z} \pd{ict} (-iE_z) \\
    &+ \pd{ict} \pd{x} iE_x + \pd{ict} \pd{y} iE_y + \pd{ict} \pd{z} iE_z \\
  =\,  
    &\pd{y} \pd{z} H_x + \pd{z} \pd{y} (-H_x) + \pd{x} \pd{z} (-H_y) + \pd{z} \pd{x} H_y \\
    &+ \pd{x} \pd{y} H_z + \pd{y} \pd{x} (-H_z) + \pd{x} \pd{ict} (-iE_x) + \pd{ict} \pd{x} iE_x \\
    &+ \pd{y} \pd{ict} (-iE_y) + \pd{ict} \pd{y} iE_y + \pd{z} \pd{ict} (-iE_z) + \pd{ict} \pd{z} iE_z \\
  =\,  &0
\end{align*}

So therefore:

\[
  \pdv{\rho}{t} + \div \vec{J} = 0
\]

In its four dimensional form:

\[
  \div \vec{J} = 0
\]

In other words, the total four dimensional divergence of the current is equal to
0, and all changes in space would necessarily be counterbalanced by a change in
time equal and opposite.

\prompt{(h) Show that the Lorentz Force Law is}

\[
  f_i = \frac1{c} F_{ij} J_k, \qquad i=1,2,3
\]

\prompt{where $f_i$ are the three components of the Lorentz force. Interpret the
quantity $icf_4$.}

Lorentz force is given by:

\[
  \vec{f} = q(\vec{E} + \vec{v} \cross \vec{H})
\]

Simplifying the given equation:

\begin{align*}
  f_i &= \frac1{c} F_{ij} J_k \\
  f_x &= \frac1{c} (0 \cdot J_x + H_z J_y - H_y J_z - i E_x J_4) \\
  &= \frac1{c} ({(\vec{H} \cross \vec{J})}_x + c \rho E_x) \\
  &= \frac1{c} {(\vec{H} \cross \vec{J})}_x + \rho E_x \\
  \vec{f} &= \frac1{c} \vec{H} \cross \vec{J} + \rho \vec{E}
\end{align*}

If we take $\tfrac{\vec{J}}{c}$ to be $\vec{v}$ and $\rho$ to be equal to $q$
then the two are equivalent.

The quantity $icf_4$ is:

\begin{align*}
  icf_4 &= ic (iE_x J_x + iE_y J_y + iE_z J_z) \\
  icf_4 &= -c \vec{E} \vdot \vec{J} \\
\end{align*}

Which indicate when the current is flowing mostly against the electric field
this value is high.  So it can be roughly thought of as a measure in the change
of potential energy.
