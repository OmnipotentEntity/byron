\prompt{(a) A charge $q$ is uniformly distributed upon a circle that has radius
$R$, is concentric wit hthe $z$-axis, and lies in a plane $z=0$.  A charge
($-q$) is located at the center of the circle.  Find (i) the electric monopole
moment, (ii) the electric-dipole moment vector and (iii) the electric-quadrupole
moment tensor, expressing the latter in an appropriate 3 by 3 array.  Using the
symmetry of the quadrupole tensor and the fact that its trace vanishes, as well
as the inherent symmetry of the physical system under consideration, one can
reduce the labor enormously.}

From the geometry of the problem, it should be remarked that the center of the
circle happens to be at the origin.  And indeed, even if it were not, because of
the total charge of the system being 0, we could relocate it there without loss
of generality, by the result of Problem~\rprob{1}{15}.

(i) Monopole moment:

\[
  q = \pint[\iiint]{\barmath{V}}{}{\rho(\vec{r})}{\barmath{V}} = +q -q = 0
\]

\medskip

(ii) Dipole moment vector:

\[
  \vec{p} = \pint[\iiint]{\barmath{V}}{}{\vec{r} \rho(\vec{r})}{\barmath{V}}
\]

The $-q$ charge at the center has $\vec{r} = \vec{0}$.  So it does not
contribute to the moment.

The infinitesimal charge along the circle at $\phi$ degrees from the $x$-axis
will be cancelled out by the same magnitude charge at $\phi + \pi$ degrees
from the circle.  So due to symmetry:

\[
  \vec{p} = \vec{0}
\]

\medskip

(iii) Quadrupole moment tensor:

\[
  Q_{ij} = \pint[\iiint]{\barmath{V}}{}{(3 x_i x_j - \lkdelta{ij} r^2) \rho(\vec{r})}{\barmath{V}}
\]

Again, because $-q$ is a charge located at $\vec{r} = \vec{0}$ it does not
contribute to the quadrupole moment.

The integral can be reduced to a single integral over $\phi$ at a fixed
distance $R$.  The value $x_i x_j$ can be expressed as:

\begin{align*}
  x_i &= R \cos \phi \\
  x_j &= R \sin \phi \\
  x_i x_j &= (R \cos \phi) (R \sin \phi) \\
  &= R^2 \sin \phi \cos \phi \\
  &= \frac{R^2}{2} \sin 2\phi
\end{align*}

When $i = 1$ and $j = 2$ or vice versa.

When $i = 3$ or $j = 3$ this value is 0 because all charge resides on the plane
$z = 0$.

And $\rho(\vec{R})$ can be given as the charge density on the line, where $q$ is
spread over a length of $2\pi R$.

If we take $x_i' = \tfrac{x_i}{R}$ then we have the integral:

\begin{align*}
  Q_{ij} &= \pint[\iiint]{\barmath{V}}{}{(3 x_i x_j - \lkdelta{ij} r^2) \rho(\vec{r})}{\barmath{V}} \\
  &= \pint{0}{2\pi}{3 x_i x_j - \lkdelta{ij} R^2) \frac{q}{2\pi R}}{\phi} \\
  &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 x_i' x_j'}{\phi} - qR \lkdelta{ij} \\
\end{align*}

For $i = j = 1$:

\begin{align*}
  Q_{ij} &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 x_i' x_j'}{\phi} - qR \lkdelta{ij} \\
  &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 \cos^2 \phi }{\phi} - qR \\
  &= \frac{qR}{2\pi} (3\pi) - qR \\
  &= \frac{3}{2} qR - qR \\
  &= \frac{qR}{2}
\end{align*}

For $i = j = 2$:

\begin{align*}
  Q_{ij} &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 x_i' x_j'}{\phi} - qR \lkdelta{ij} \\
  &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 \sin^2 \phi }{\phi} - qR \\
  &= \frac{qR}{2\pi} (3\pi) - qR \\
  &= \frac{3}{2} qR - qR \\
  &= \frac{qR}{2}
\end{align*}

For $i = 1$ and $j = 2$ or vice versa:

\begin{align*}
  Q_{ij} &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 x_i' x_j'}{\phi} - qR \lkdelta{ij} \\
  &= \frac{qR}{2\pi} \pint{0}{2\pi}{\frac{3}{2} \sin 2\phi }{\phi} \\
  &= \frac{qR}{2\pi} 0 \\
  &= 0
\end{align*}

For $i = 3$ or $j = 3$ but not $i = j$:

\begin{align*}
  Q_{ij} &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 x_i' x_j'}{\phi} - qR \lkdelta{ij} \\
  &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 \cdot 0}{\phi} \\
  &= 0
\end{align*}

For $i = j = 3$, because the matrix is traceless, this must be $-qR$.  But it is trivial to check:

\begin{align*}
  Q_{ij} &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 x_i' x_j'}{\phi} - qR \lkdelta{ij} \\
  &= \frac{qR}{2\pi} \pint{0}{2\pi}{3 \cdot 0}{\phi} - qR \\
  &= -qR
\end{align*}


So overall the tensor is:

\[
  \mat{Q}_{ij} = \begin{bmatrix}
    \frac{qR}{2} & 0 & 0 \\
    0 & \frac{qR}{2} & 0 \\
    0 & 0 & -qR \\
  \end{bmatrix}
\]

\prompt{(b) Determine for this charge distribution the multipole expansion for
the potential at distance $r$ from the origin for which $(r/R) \gg 1$; carry the
expansion through the quadrupole contribution, and express the spatial
dependence in the final result in terms of the spherical corrdinates $r, \theta,
\phi$.}

The coordinates of the point being examined can be expressed using (taking the
common physics convention of $\theta$ as the zenithal angle and $\phi$ as the
azimuthal angle)

\begin{align*}
  x_1 &= r \sin \theta \cos \phi \\
  x_2 &= r \sin \theta \sin \phi \\
  x_3 &= r \cos \theta.
\end{align*}

We can express the potential using the multipole expansion.

\begin{align*}
  \varphi(\vec{r}) &= \frac1{r} \pint{\tau'}{}{\rho(\vec{r'})}{\tau'} + \frac{\vec{r}}{r^3} \vdot \pint{\tau'}{}{\vec{r'} \rho(\vec{r'})}{\tau'} \\
      &+ \frac{x_i x_j}{2 r^5} \pint{\tau'}{}{(3 x_i' x_j' - \lkdelta{ij}r'^2)\rho(\vec{r'})}{\tau'} \ldots
\end{align*}

The first two terms of this expression are 0 because $q$ and $\vec{p}$ are, so
focusing on the quadrupole contribution:

\begin{align*}
  \varphi(\vec{r}) &= \frac{x_i x_j}{2 r^5} Q_{ij} \\
  &= \sum_{i, j} \frac{x_i x_j}{2 r^5} Q_{ij}
\end{align*}

Because $Q_{ij}$ is only non-zero along the diagonal.

\begin{align*}
  \varphi(\vec{r}) &= \sum_{i} \frac{x_i x_i}{2 r^5} Q_{ii} \\
  &= \frac{qR}{4r^5} (r^2 \sin^2 \theta \cos^2 \phi + r^2 \sin^2 \theta \sin^2 \phi - 2 r^2 \cos^2 \theta) \\
  &= \frac{qR}{4r^3} (1 - 3 \cos^2 \theta)
\end{align*}
