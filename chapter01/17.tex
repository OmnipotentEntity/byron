\prompt{Find the first three multipole moments for a single point charge $q$
sitting at the point $\vec{r} = R(\ihat + \jhat - \khat)$. Determine, through
three terms in the multipole expansion, the potential (in spherical coordinates)
at a point very far away from the charge.}

First, let's define the function of charge concentration, using the Dirac Delta
function:

\[
  \rho(\vec{r}) = q\delta\qty(\vec{x} - \begin{bmatrix}R \\ R \\ -R\end{bmatrix})
\]

Monopole moment:

\[
  \pint[\iiint]{\barmath{V}}{}{\rho(\vec{r})}{\barmath{V}} = q
\]

Dipole moment:

\begin{align*}
  \vec{p} &= \pint[\iiint]{\barmath{V}}{}{\rho(\vec{r})}{\barmath{V}} \\
  &= \begin{bmatrix} Rq \\ Rq \\ -Rq \end{bmatrix}
\end{align*}

Quadrupole moment:

\[
  Q_{ij} = \pint[\iiint]{\barmath{V}}{}{(3 x_i x_j - \delta_{ij} r^2) \rho(\vec{r})}{\barmath{V}}
\]

\begin{align*}
  Q_{11} &= 3(R)(R) - \qty(R\sqrt{3})^2 &&= 0 \\
  Q_{12} &= (3(R)(R) - 0) &&= 3qR^2 \\
  Q_{13} &= (3(R)(-R) - 0) &&= -3qR^2 \\
  Q_{21} &= Q_{12} &&= 3qR^2 \\
  Q_{22} &= Q_{11} &&= 0 \\
  Q_{23} &= (3(R)(-R) - 0) &&= -3qR^2 \\
  Q_{31} &= Q_{13} &&= -3qR^2 \\
  Q_{32} &= Q_{23} &&= -3qR^2 \\
  Q_{33} &= 3(-R)(-R) - \qty(R\sqrt{3})^2 &&= 0 \\
\end{align*}

So:

\[
  \mat{Q}_{ij} = \begin{bmatrix}
    0 & 3qR^2 & -3qR^2 \\
    3qR^2 & 0 & -3qR^2 \\
    -3qR^2 & -3qR^2 & 0
  \end{bmatrix}
\]

Finally, we take the multipole expansion.

First, the coordinates of the point $\vec{r}$ can be expressed using (taking the
common physics convention of $\theta$ as the zenithal angle and $\phi$ as the
azimuthal angle)

\begin{align*}
  x_1 &= r \sin \theta \cos \phi \\
  x_2 &= r \sin \theta \sin \phi \\
  x_3 &= r \cos \theta.
\end{align*}

Next, each term may be written out:

\begin{align*}
  \varphi(\vec{r}) &= \frac1{r} \pint{\tau'}{}{\rho(\vec{r'})}{\tau'} + \frac{\vec{r}}{r^3} \vdot \pint{\tau'}{}{\vec{r'} \rho(\vec{r'})}{\tau'} \\
      &+ \frac{x_i x_j}{2 r^5} \pint{\tau'}{}{(3 x_i' x_j' - \delta_{ij}r'^2)\rho(\vec{r'})}{\tau'} \ldots \\
  &= \frac{q}{r} + \frac1{r^3} r \begin{bmatrix} \sin \theta \cos \phi \\ \sin \theta \sin \phi \\ \cos \theta \end{bmatrix} \vdot \begin{bmatrix} qR \\ qR \\ -qR \end{bmatrix} \\
    &+ \frac{r^2}{2r^5} \sum_{i,j} {\begin{bmatrix} 
    \sin^2 \theta \cos^2 \phi & \sin^2 \theta \sin \phi \cos \phi & \sin \theta \cos \theta \cos \phi \\
    \sin^2 \theta \sin \phi \cos \phi & \sin^2 \theta \sin^2 \phi & \sin \theta \cos \theta \sin \phi \\
    \sin \theta \cos \theta \cos \phi & \sin \theta \cos \theta \sin \phi & \cos^2 \theta \\
  \end{bmatrix}}_{ij}
  {\begin{bmatrix}
    0 & 3qR^2 & -3qR^2 \\
    3qR^2 & 0 & -3qR^2 \\
    -3qR^2 & -3qR^2 & 0
  \end{bmatrix}}_{ij} \\
  &= \frac{q}{r} + \frac{qR}{r^2} \qty(\sin \theta \cos \phi + \sin \theta \sin \phi - \cos \theta) \\
  &+ \frac{3qR^2}{2r^3} \qty(2 \sin^2 \theta \sin \phi \cos \phi - 2 \sin \theta \cos \theta \cos \phi - 2 \sin \theta \cos \theta \sin \phi) \\
  &= \frac{q}{r} + \frac{qR}{r^2} \qty(\sin \theta \cos \phi + \sin \theta \sin \phi - \cos \theta) \\
  &+ \frac{3qR^2}{r^3} \qty(\sin^2 \theta \sin \phi \cos \phi - \sin \theta \cos \theta \cos \phi - \sin \theta \cos \theta \sin \phi) \\
\end{align*}
