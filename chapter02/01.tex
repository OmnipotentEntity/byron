\prompt{The shortest line (polar coordinates). Find the shortest distance between two points using polar coordinates, i. e., using them as a line element:}

\[
  \dd{s}^2 = \dd{r}^2 + r^2\dd{\theta}^2
\]

Using the line element, our function to optimize is:

\begin{align*}
  f(\theta; r; r') &= \dv{s}{x} \\
  &= \sqrt{r'^2 + r^2}
\end{align*}

Because it is not dependent upon theta, we can use the simplified form of the
Euler-Lagrange equation.

\[
  r' \pdv{f}{r'} - f = C_1
\]

Note that

\[
  \pdv{f}{r'} = \frac{r'}{\sqrt{r'^2 + r^2}}.
\]

So we have:

\begin{align}
  \frac{r'^2}{\sqrt{r'^2 + r^2}} - \sqrt{r'^2 + r^2} &= C_1 \nonumber \\
  r'^2 - r'^2 - r^2 &= C_1 \sqrt{r'^2 + r^2} \nonumber \\
  r^4 &= C_1(r'^2 + r^2) \nonumber \\
  r' = \dv{r}{\theta} &= C_1r \sqrt{r^2 - C_1^2} \leqn{separable}
\end{align}

Equation~\reqn{separable} is a separable differential equation.

\begin{align}
  \pint{}{}{\frac{C_1}{r\sqrt{r^2 - C_1^2}}}{r} &= \pint{}{}{}{\theta} \nonumber \\
  u &= \sqrt{r^2 - C_1^2} \nonumber \\
  r^2 &= u^2 + C_1^2 \nonumber \\
  r &= \sqrt{u^2 + C_1^2} \nonumber \\
  \dd{u} &= \frac{r}{\sqrt{r^2 - C_1^2}} \dd{r} \nonumber \\
  \dd{r} &= \frac{u}{\sqrt{u^2 + C_1^2}} \dd{u} \nonumber \\
  C_1 \pint{}{}{\frac{u}{\sqrt{u^2 + C_1^2}\sqrt{u^2 + C_1^2} u}}{u} &= \theta + C_2 \nonumber \\
  C_1 \int \frac{\dd{u}}{u^2 + C_1^2} &= \tan^{-1}\qty(\frac{u}{C_1}) \nonumber \\
  \tan^{-1}\qty(\frac{u}{C_1}) &= \theta + C_2 \nonumber \\
  \frac{\sqrt{r^2 - C_1^2}}{C_1} &= \tan(\theta + C_2) \nonumber \\
  r^2 &= C_1^2\qty(1 + \tan^2(\theta + C_2)) \nonumber \\
  r^2 &= C_1^2 \sec^2(\theta + C_2) \nonumber \\
  r &= C_1 \sec(\theta + C_2) \leqn{straight-line-polar}
\end{align}

And equation~\reqn{straight-line-polar} is an equation for a straight line
in polar coordinates.  To wit

\begin{align*}
  r &= C_1 \sec(\theta + C_2) \\
  r \cos(\theta + C_2) &= C_1 \\
  r \qty(\cos \theta \cos C_2 - \sin \theta \sin C_2) &= C_1 \\
  x \cos C_2 - y \sin C_2 &= C_1 \\
  y &= x \cot C_2 - C_1 \csc C_2 \\
  y &= mx + b.
\end{align*}

Therefore, the shortest distance between two points in a plane is a straight
line, even in polar coordinates.
