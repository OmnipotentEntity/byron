\prompt{In all our discussions so far on finding the function $f$ for which $I =
\pint{x_1}{x_2}{f}{x}$ is an extremum, it has been assumed that $f$ depends only
on $x$, $y$, and $y'$:}

\[
  f = f(x, y, y').
\]

\prompt{Show that if $f$ also involves the second derivative of $y$ with respect
to $x$, $f = f(x, y, y', y'')$, then for fixed endpoints and prescribed $y'$ at
the endpoints, the Euler-Lagrange equation is}

\[
  \pdv{f}{y} - \dv{x}(\pdv{f}{y'}) + \dv[2]{x}(\pdv{f}{y''}) = 0.
\]

As a reminder, in the standard Euler-Lagrange equation $f$ is a
twice-differentiable function of all its arguments.  Additionally, we can
define $y(x_1) = y_1$, $y(x_2) = y_2$, because of the fixity of the endpoints.
A function $\bar{y}(x, \epsilon)$ is defined with the following properties:

\begin{align}
  \bar{y}(x_1, \epsilon) &= y_1, \qquad \bar{y}(x_2, \epsilon) = y_2 \qqtext{for all $\epsilon$} \leqn{vanishing} \\
  \bar{y}(x, 0) &= y(x) \leqn{optimized} \\
  \bar{y}(x, \epsilon)& \text{and its derivatives through second order are continuous functions of $x$ and $\epsilon$} \leqn{continuous}
\end{align}

Without imposing more conditions, let's attempt to create the appropriate
integral and simplify it for now.

\begin{align*}
  I(\epsilon) &= \pint{x_1}{x_2}{f(x, \bar{y}, \bar{y}', \bar{y}'')}{x} \\
  \dv{I}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}} \dv{\bar{y}}{\epsilon}
    + \pdv{f}{\bar{y}'} \dv{\bar{y}'}{\epsilon}
    + \pdv{f}{\bar{y}''} \dv{\bar{y}''}{\epsilon}]}{x} \\
  &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}} \dv{\bar{y}}{\epsilon}
    + \pdv{f}{\bar{y}'} \dv{x}(\dv{\bar{y}}{\epsilon})
    + \pdv{f}{\bar{y}''} \dv[2]{x}(\dv{\bar{y}''}{\epsilon})]}{x} \\
\end{align*}

At this point, in the original derivation integration by parts occurs.  It'd be
useful to develop a general way of doing integration by parts on a second order
derivative.  Taking a moment to do that gives:

\begin{align*}
  \dv[2]{x}(uv) &= \dv{x}(\dv{x}(uv)) \\
  &= \dv{x}(\dv{u}{x} v + u \dv{v}{x}) \\
  &= \dv{x}(\dv{u}{x} v) + \dv{x}(u \dv{v}{x}) \\
  &= \dv[2]{u}{x} v + 2 \dv{u}{x} \dv{v}{x} + u \dv[2]{v}{x} \\
  \pint{}{}{u \dv[2]{v}{x}}{x} &= \dv{x}(uv) - \pint{}{}{\dv[2]{u}{x} v}{x} - 2\pint{}{}{\dv{u}{x}\dv{v}{x}}{x}.
\end{align*}

Applying integration by parts to the original integral:

\begin{align*}
  \dv{I}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}} \dv{\bar{y}}{\epsilon}
    + \pdv{f}{\bar{y}'} \dv{x}(\dv{\bar{y}}{\epsilon})
    + \pdv{f}{\bar{y}''} \dv[2]{x}(\dv{\bar{y}''}{\epsilon})]}{x} \\
  \dv{I_1}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}} \dv{\bar{y}}{\epsilon}]}{x} \\
  \dv{I_2}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}'} \dv{x}(\dv{\bar{y}}{\epsilon})]}{x} \\
  \dv{I_3}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}''} \dv[2]{x}(\dv{\bar{y}}{\epsilon})]}{x} \\
\end{align*}

Let's take care of $I_2$ first, whose derivation is the same as in the book.

\begin{align*}
  \dv{I_2}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}'} \dv{x}(\dv{\bar{y}}{\epsilon})]}{x} \\
  &= {\qty[\dv{\bar{y}}{\epsilon} \pdv{f}{\bar{y}'}]}_{x_1}^{x_2} - \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv{x}(\pdv{f}{\bar{y}'})}{x}
\end{align*}

The first term of this goes to zero because of Equation~\reqn{vanishing} makes $\tfrac{\dd{\bar{y}}}{\dd{\epsilon}} = 0$ at $x_1$ and $x_2$.

Next $I_3$:

\begin{align}
  \dv{I_3}{\epsilon} &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}''} \dv[2]{x}(\dv{\bar{y}}{\epsilon})]}{x} \nonumber \\
  &= {\qty[\dv{x}(\dv{\bar{y}}{\epsilon}\pdv{f}{\bar{y}''})]}_{x_1}^{x_2}
    - \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x}
    - 2 \pint{x_1}{x_2}{\dv{x}(\dv{\bar{y}}{\epsilon}) \dv{x}(\pdv{f}{\bar{y}''})}{x} \nonumber \\
  \dv{I_{3,1}}{\epsilon} &= {\qty[\dv{x}(\dv{\bar{y}}{\epsilon}\pdv{f}{\bar{y}''})]}_{x_1}^{x_2} \nonumber \\
  &= {\qty[\dv{x}(\dv{\bar{y}}{\epsilon})\pdv{f}{\bar{y}''} + \dv{\bar{y}}{\epsilon}\dv{x}(\pdv{f}{\bar{y}''})]}_{x_1}^{x_2} \nonumber \\
  &= {\qty[\dv{x}(\dv{\bar{y}}{\epsilon})\pdv{f}{\bar{y}''}]}_{x_1}^{x_2} \nonumber \\
  \dv{I_{3,3}}{\epsilon} &= 2 \pint{x_1}{x_2}{\dv{x}(\dv{\bar{y}}{\epsilon}) \dv{x}(\pdv{f}{\bar{y}''})}{x} \nonumber \\
  &= 2 {\qty[\dv{\bar{y}}{\epsilon}\dv[2]{x}(\pdv{f}{\bar{y}''})]}_{x_1}^{x_2}
    - 2 \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x} \nonumber \\
  &= - 2 \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x} \nonumber \\
  \dv{I_3}{\epsilon} &= {\qty[\dv{x}(\dv{\bar{y}}{\epsilon})\pdv{f}{\bar{y}''}]}_{x_1}^{x_2}
    - \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x}
    + 2 \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x} \nonumber \\
  &= {\qty[\dv{\bar{y}'}{\epsilon} \pdv{f}{\bar{y}''}]}_{x_1}^{x_2}
    + \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x} \leqn{I3}
\end{align}

Reviewing the remaining work to develop the Euler-Lagrange equation, it is clear
that the additional condition to be imposed must be:

\begin{equation}
  \bar{y}'(x_1, \epsilon) = y'_1, \qquad \bar{y'}(x_2, \epsilon) = y'_2 \qqtext{for all $\epsilon$} \leqn{vanishing2}
\end{equation}

Or that $\bar{y}'$ has a vanishing condition with respect to $\epsilon$ at the
fixed endpoints, just like $\bar{y}$.  This allows the first term in
Equation~\reqn{I3} to be discarded and for work to proceed.

So all together, after making the required cancelations, we have:

\begin{align*}
  \dv{I}{\epsilon} &= \pint{x_1}{x_2}{\pdv{f}{\bar{y}} \dv{\bar{y}}{\epsilon}}{x}
    - \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv{x}(\pdv{f}{\bar{y}'})}{x}
    + \pint{x_1}{x_2}{\dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x} \\
  &= \pint{x_1}{x_2}{\pdv{f}{\bar{y}} \dv{\bar{y}}{\epsilon}
    - \dv{\bar{y}}{\epsilon} \dv{x}(\pdv{f}{\bar{y}'})
    + \dv{\bar{y}}{\epsilon} \dv[2]{x}(\pdv{f}{\bar{y}''})}{x} \\
  &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}} - \dv{x}(\pdv{f}{\bar{y}'})
    + \dv[2]{x}(\pdv{f}{\bar{y}''})] \dv{\bar{y}}{\epsilon}}{x}
\end{align*}

We require $I(\epsilon)$ to have a minimum at $\epsilon = 0$, so

\begin{align*}
  \left.\dv{I}{\epsilon}\right|_{\epsilon = 0} &= 0 \\
  &= \pint{x_1}{x_2}{\qty[\pdv{f}{\bar{y}} - \dv{x}(\pdv{f}{\bar{y}'})
    + \dv[2]{x}(\pdv{f}{\bar{y}''})] \dv{\bar{y}}{\epsilon}}{x}.
\end{align*}

If we let

\[
  \left.\dv{\bar{y}}{\epsilon}\right|_{\epsilon = 0} = \eta(x),
\]

then because of Equation~\reqn{optimized}, we know that at $\epsilon = 0$, $y =
\bar{y}$ so

\[
  \left.\dv{I}{\epsilon}\right|_{\epsilon = 0} = \pint{x_1}{x_2}{\qty[\pdv{f}{y} - \dv{x}(\pdv{f}{y'})
    + \dv[2]{x}(\pdv{f}{y''})] \eta(x)}{x}.
\]

And because $\eta(x)$, other than from the conditions imposed in
Equations~\reqn{vanishing} and~\reqn{continuous}, is an arbitrary function of
$x$, in order for the integral to vanish it must be the case that

\[
  \pdv{f}{y} - \dv{x}(\pdv{f}{y'}) + \dv[2]{x}(\pdv{f}{y''}) = 0.
\]

Because this derivation makes assumptions about the nature of $y''$ it requires
that $y$ have an existing \nth{3} derivative.  This is a change from the
standard derivation, which only requried a \nth{2}.
