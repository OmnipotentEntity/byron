\prompt{Fermat's principle states: If the velocity of light is given by the
continuous function $u = u(y)$, the actual light path connecting the points
$(x_1, y_1)$ and $(x_2, y_2)$ in a plane is one which extremizes the time
integral}

\[
  I = \fpint{(x_1, y_1)}{(x_2, y_2)}{}{u}{s}.
\]

\prompt{(Actually, refinements are needed to make this formulation of Fermat's
principle hold for all cases.)}

\prompt{(a) Derive Snell's law from Fermat's principle; that is, prove that
$\tfrac{\sin \phi}{u} =$ const, where $\phi$ is the angle shown in
Fig.~\rfig{curve}}

\begin{figure}[!ht]
\begin{center}
\begin{tikzpicture}
  % axes
  \draw (-1,2) -- (10,2);
  \draw (0,1) -- (0,9);
  % curve
  \draw (1,3) .. controls +(60:3) and +(135:3) .. (8,6);
  \draw [fill=black] (1,3) circle (0.04);
  \draw [fill=black] (8,6) circle (0.04);
  % orthogonal axes
  \draw (2.8,6) -- (4.8,6);
  \draw (3.8,5) -- (3.8,7);
  % tangent line
  \draw (2.8,5.35) -- (4.8,6.65);
  % angle
  \draw (3.8, 6.25) arc [radius=0.25, start angle=90, end angle=34];
  % labels
  \node at (4.05, 6.45) {$\phi$};
  \node [below right] at (10, 2) {$x$};
  \node [above left] at (0, 9) {$y$};
  \node [below right] at (1,3) {$(x_1, y_1)$};
  \node [below right] at (8,6) {$(x_2, y_2)$};
\end{tikzpicture}
\caption{Showing the orientation of $\phi$ with respect to the path of light.}
\lfig{curve}
\end{center}
\end{figure}

As a reminder,

\[
  \dv{s}{x} = \sqrt{1 + y'^2}.
\]

So the integral to be extremized is

\[
  I = \fpint{(x_1, y_1)}{(x_2, y_2)}{\sqrt{1 + y'^2}}{u(y)}{x}.
\]

Using the simplified form of the Euler-Lagrange equation

\begin{align*}
  y'\qty(\pdv{f}{y'}) - f &= C_1 \\
  \frac{y'^2}{u(y) \sqrt{1 + y'^2}} - \frac{\sqrt{1 + y'^2}}{u(y)} &= C_1 \\
  - \frac1{\sqrt{1 + y'^2}} &= C_1 u(y) \\
\end{align*}

From Figure~\rfig{curve}, $\phi$ represents the angle of a particular right
triangle whose opposite side is $\dd{x}$ and whose hypotenuse is $\dd{s}$.  Or
more specifically

\begin{align*}
  \sin \phi &= \dv{x}{s} \\
  &= \frac1{\sqrt{1 + y'^2}}.
\end{align*}

So

\begin{align*}
  - \sin \phi &= u(t) C_1 \\
  \frac{\sin \phi}{u} &= C \\
\end{align*}

\prompt{(b) Suppose that light travels in the $xy$-plane in such a way that its
speed is proportional to $y$; then prove that the light rays emitted from any
point are circles with their centers on the $x$-axis.}

If $u(y)$ is proportional to $y$ then it has the form $u(y) = ky$.  Then, after
combining constants,

\[
  \frac{\sin \phi}{y} = C.
\]

Undoing the final step of the last section:

\begin{align*}
  \sin \phi &= \dv{x}{s} = \frac{\dd{x}}{\sqrt{\dd{x}^2 + \dd{y}^2}} \\
  \frac{\sin \phi}{y} &= C \\
  \dd{x} &= C y \sqrt{\dd{x}^2 + \dd{y}^2} \\
  \dd{x}^2 &= C^2 y^2 (\dd{x}^2 + \dd{y}^2) \\
  \dd{x}^2 &= \frac{C^2 y^2}{1 - C^2 y^2} \dd{y}^2 \\
  \dd{x} &= \frac{C y}{\sqrt{1 - C^2 y^2}} \dd{y} \\
  (x - x_0) &= \frac{\sqrt{1 - C^2 y^2}}{C} \\
  C^2 (x - x_0)^2 &= 1 - C^2 y^2 \\
  (x - x_0)^2 + y^2 &= \frac1{C^2} = r^2.
\end{align*}

Which is the equation for a circle with center on the $x$-axis.
