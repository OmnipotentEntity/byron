\prompt{Brachistochrone problem for a central gravitational force. A particle of
mass $m$ is to move under the gravitational attraction of another stationary
spherical mass $M$ from a point at a finite distance $r = R_0$ from the center
of $M$ to a point on the surface of $M$.  We want to determine the path such
that the particle moving along it will travel between the two fixed endpoints in
a time less than the time of transit along any other path.  Taking for the two
endpoints ($\theta = 0$, $r = R_0$) and ($\theta = \alpha$, $r = R$) as shown in
Figure~\rfig{central_brach}, set up in polar coordinates the equation that
determines the path and solve for $\theta$ as a function of $r$.  The best one
can do analytically is to express $\theta$ as an elliptic integral with a
variable upper limit and lower limit chosen to satisfy one of the boundary
conditions.  The determination of this path tells us how best to design
interplanetary mail chutes.}

\begin{figure}[!ht]
\begin{center}
\begin{tikzpicture}
  % axes
  \draw (-2, 0) -- (10, 0);
  \draw (0,-2) -- (0, 2);
  % circle
  \draw (0, 0) circle(1.5);
  % curve
  \draw (0.75, 1.299) .. controls +(-30:3) and +(180:3) .. (9, 0);
  \draw [fill=black] (0.75, 1.299) circle (0.04);
  \draw [fill=black] (9, 0) circle (0.04);
  % angle
  \draw (0, 0) -- (0.75, 1.299);
  \draw (0.5, 0)[->] arc [radius=0.5, start angle=0, end angle=60];
  % distance axis
  \draw (9, 0) -- (9, -1);
  \draw[<->] (0, -0.5) -- (9, -0.5);
  % labels
  \node at (0.6062, 0.35) {$\alpha$}; % (30 degrees at radius 0.7)
  \node at (0.4104, 1.128) {$R$}; % (70 degrees at radius 1.2)
  \node at (-1.27279, 1.27279) {$M$}; % (135 degrees at radius 1.8)
  \node at (4.5, -1) {$R_0$};

\end{tikzpicture}
\caption{A possible path of the ``mail chute.''}
\lfig{central_brach}
\end{center}
\end{figure}

It's possible to relate the velocity to the radius using a basic conservation of energy equation:

\begin{align*}
  E &= U + T \\
    &= - GMm \qty(\frac1{r} - \frac1{r_0}) + \frac1{2} mv^2 \\
\end{align*}

Without loss of generality, we can set the zero energy condition at $r_0$, giving:

\begin{align*}
  U &= - \frac{GMm}{r} \\
  v &= \sqrt{\frac{2GM}{r}}
\end{align*}

The line element in polar coordinates is given by:

\[
  \dd{s}^2 = r^2 \dd{\theta}^2 + \dd{r}^2
\]

Giving $r$ as a function of $\theta$.

\[
  \dv{s}{r} = \sqrt{1 + r^2 \theta'^2}
\]

And similarly to the Brachistochrone example in the book, the integral to be extremized is:

\begin{align*}
  I &= \fpint{A}{B}{}{v}{s} \\
    &= \pint{A}{B}{\frac{\sqrt{1 + r^2 \theta'^2}}{v}}{r} \\
    &= \pint{A}{B}{\frac{\sqrt{1 + r^2 \theta'^2}}{\sqrt{\frac{2GM}{r}}}}{r} \\
    &= \sqrt{\frac1{2GM}} \pint{A}{B}{\sqrt{r + r^3 \theta'^2}}{r} \\
\end{align*}

So in this case the extremization function is:

\[
  f(r, \theta, \theta') = \sqrt{r + r^3 \theta'^2}
\]

A factor of $1/\sqrt{2GM}$ was dropped from f, because it is a constant with
respect to all variables and it can be added in later (but it will not need to
be as we will see shortly).

Because $f$ is an explicit function of $r$ it is not possible to use the
simplified Euler-Legrange equation.  Instead the original version must be used.
However, because this function is \textit{not} an explicit function of $\theta$ a
very nice simplification is possible.

\begin{align*}
  \pdv{f}{\theta} - \dv{r}(\pdv{f}{\theta'}) &= 0 \\
  0 - \dv{r}(\pdv{f}{\theta'}) &= 0 \\
  \pdv{f}{\theta'} &= C_1 \\
\end{align*}

Finally, insert the value of $f$ into this equation and simplify and solve for
$\theta'$:

\begin{align*}
  \pdv{f}{\theta'} &= C_1 \\
  \pdv{\theta'}(\sqrt{r + r^3 \theta'^2}) &= C_1 \\
  \frac{r^3 \theta'}{\sqrt{r + r^3 \theta'^2}} &= C_1 \\
  \theta' &= \pm \frac{C_1}{\sqrt{r^5 - C_1^2 r^2}}
\end{align*}

Despite the books claims, it is possible to integrate this function (though it
is a bit involved.)  The result is:

\[
  \theta(r) = \pm \frac{2}{3} \tan^{-1} \qty(\frac{\sqrt{r^3 - C_1^2}}{C_1}) + C_2
\]

Finding the proper value of $C_1$ and $C_2$ for the end points is a matter of
solving the set of equations:

\begin{align*}
  \theta(r_0) &= 0 \\
  \theta(R) &= \alpha \\
\end{align*}

Unfortunately, this step seems to be intractible to solve symbolically.  I
suppose interplanetary mail chutes will have to wait.
