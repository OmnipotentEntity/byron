\prompt{a. A charged scalar meson is described by two real fields
$\varphi_1(x_\mu)$, $\varphi_2(x_\mu)$, where $x_\mu = (x, y, z, it)$, (natural
units: $c = \hbar = 1$).  The Lagrange density for such a meson in an external
electromagnetic field, described by the vector potential $A_\mu(x)$, is}

\begin{align*}
  \lagden = &- \frac1{2} \qty(\pdv{\varphi_1}{x_\mu} \pdv{\varphi_1}{x_\mu} + \pdv{\varphi_2}{x_\mu} \pdv{\varphi_2}{x_\mu}) - \frac{m^2}{2} \qty(\varphi_1^2 + \varphi_2^2) \\
    &+ e A_\mu \qty[ \varphi_2 \pdv{\varphi_1}{x_\mu} - \varphi_1 \pdv{\varphi_2}{x_\mu} ] + \frac1{2} e^2 A_\mu A_\mu \qty(\varphi_1^2 + \varphi_2^2).
\end{align*}

\prompt{Here we use Einstein summation convention (summation over repeated
indices).  Find the Euler-Lagrange equations for $\varphi_1$ and $\varphi_2$
corresponding to this Lagrangian; $e$ and $m$ are constant; $A_\mu$ is a given
function of $x$.}

For reference, the Euler-Lagrange equations are:

\[
  \pdv{\lagden}{\varphi_j} - \pdv{x_\mu}(\pdv{\lagden}{\pdv{\varphi_j}{x_\mu}}) = 0
\]

Taking each term in turn:

\begin{align*}
  \pdv{\lagden}{\varphi_1} &= \qty(e^2 A_\mu A_\mu - m^2) \varphi_1 - e A_\mu \pdv{\varphi_2}{x_\mu} \\
  \pdv{\lagden}{\varphi_2} &= \qty(e^2 A_\mu A_\mu - m^2) \varphi_2 + e A_\mu \pdv{\varphi_1}{x_\mu} \\
  \pdv{\lagden}{\pdv{\varphi_1}{x_\mu}} &= - \pdv{\varphi_2}{x_\mu} + e A_\mu \varphi_2 \\
  \pdv{x_\mu}(\pdv{\lagden}{\pdv{\varphi_1}{x_\mu}}) &= - \pdv[2]{\varphi_2}{x_\mu} + e \qty(\pdv{A_\mu}{x_\mu} \varphi_2 + A_\mu \pdv{\varphi_2}{x_\mu}) \\
  \pdv{\lagden}{\pdv{\varphi_2}{x_\mu}} &= - \pdv{\varphi_1}{x_\mu} - e A_\mu \varphi_1 \\
  \pdv{x_\mu}(\pdv{\lagden}{\pdv{\varphi_2}{x_\mu}}) &= - \pdv[2]{\varphi_1}{x_\mu} - e \qty(\pdv{A_\mu}{x_\mu} \varphi_1 + A_\mu \pdv{\varphi_1}{x_\mu})
\end{align*}

Then putting it all together:

\begin{align*}
  0 &= \pdv{\lagden}{\varphi_1} - \pdv{x_\mu}(\pdv{\lagden}{\pdv{\varphi_1}{x_\mu}}) \\
  0 &= \qty(e^2 A_\mu A_\mu - m^2) \varphi_1 - e A_\mu \pdv{\varphi_2}{x_\mu} - \qty(- \pdv[2]{\varphi_2}{x_\mu} + e \qty(\pdv{A_\mu}{x_\mu} \varphi_2 + A_\mu \pdv{\varphi_2}{x_\mu})) \\
  0 &= \qty(e^2 A_\mu A_\mu - m^2 + \dalembertian) \varphi_1 - e \pdv{A_\mu}{x_\mu} \varphi_2
\end{align*}

The derivation is very similar for $\varphi_2$ giving the Euler-Lagrange equations for this Lagrangian Density:

\begin{align*}
  \qty(e^2 A_\mu A_\mu - m^2 + \dalembertian) \varphi_1 &= e \pdv{A_\mu}{x_\mu} \varphi_2 \\
  \qty(e^2 A_\mu A_\mu - m^2 + \dalembertian) \varphi_2 &= - e \pdv{A_\mu}{x_\mu} \varphi_1
\end{align*}

\prompt{b. Sometimes the complex fields $\varphi$ and $\conj \varphi$ are
introduced in place of the two real fields $\varphi_1$ and $\varphi_2$:}

\begin{align*}
  \varphi(x_\mu) &= \frac1{\sqrt{2}} \qty[ \varphi_1(x_\mu) - i \varphi_2(x_\mu) ], \\
  \conj \varphi(x_\mu) &= \frac1{\sqrt{2}} \qty[ \varphi_1(x_\mu) + i \varphi_2(x_\mu) ].
\end{align*}

\prompt{Show that the Lagrangian density in terms of these two new complex
dependent field variables in free space ($A_\mu = 0$) is}

\[
  \lagden_0 = - \qty(\pdv{\conj \varphi}{x_\mu} \pdv{\varphi}{x_\mu} + m^2 \conj \varphi \varphi).
\]

\prompt{Now determine the Euler-Lagrange equations for this Lagrangian density
(either independently or from part (a) of the problem by transformation of
$\varphi_1$ and $\varphi_2$ into $\varphi$ and $\conj \varphi$).  You will find
that the Euler-Lagrange equations are}

\[
  (\dalembertian - m^2) \varphi = 0, \qqtext{and} (\dalembertian - m^2) \conj \varphi = 0,
\]

\prompt{the Klein-Gordon equation which describes a spinless particles in free
space.  Here the two equations describe the two charged $\pi$-mesons.}

As a reminder, from Chapter 1, the $\dalembertian$ operator is called the
D'Alembertian, and it is defined as:

\[
  \dalembertian \equiv \laplacian - \frac1{c^2} \pdv[2]{t}.
\]

Because $A_\mu = 0$ in free space, we can rewrite the Lagrangian density as:

\[
  \lagden_0 = - \frac1{2} \qty(\pdv{\varphi_1}{x_\mu} \pdv{\varphi_1}{x_\mu} + \pdv{\varphi_2}{x_\mu} \pdv{\varphi_2}{x_\mu}) - \frac{m^2}{2} \qty(\varphi_1^2 + \varphi_2^2)
\]

Next, solve for the values of $\varphi_1$ and $\varphi_2$ in terms of $\varphi$ and $\conj \varphi$.

\begin{align*}
  \varphi &= \frac1{\sqrt{2}} \qty(\varphi_1 - i \varphi_2) \\
  \conj \varphi &= \frac1{\sqrt{2}} \qty(\varphi_1 + i \varphi_2) \\
  \varphi + \conj \varphi &= \sqrt{2} \varphi_1 \\
  \varphi_1 &= \frac1{\sqrt{2}} \qty(\varphi + \conj \varphi) \\
  \varphi - \conj \varphi &= - i \sqrt{2} \varphi_2 \\
  \varphi_2 &= \frac{i}{\sqrt{2}} \qty(\varphi - \conj \varphi)
\end{align*}

Because this is a linear equation, and the derivative is also linear, we have easily:

\begin{align*}
  \pdv{\varphi_1}{x_\mu} &= \frac1{\sqrt{2}} \qty(\pdv{\varphi}{x_\mu} + \pdv{\conj \varphi}{x_\mu}) \\
  \pdv{\varphi_2}{x_\mu} &= \frac{i}{\sqrt{2}} \qty(\pdv{\varphi}{x_\mu} - \pdv{\conj \varphi}{x_\mu})
\end{align*}

Consider part of the final term of the Lagrangian Density:

\begin{align*}
  \varphi_1^2 + \varphi_2^2 &= {\qty(\frac1{\sqrt{2}} \qty(\varphi + \conj \varphi))}^2 + {\qty(\frac{i}{\sqrt{2}} \qty(\varphi - \conj \varphi))}^2 \\
  &= \frac1{2} \qty(\varphi^2 + 2 \varphi \conj \varphi + {\conj \varphi}^2) - \frac1{2} \qty(\varphi^2 - 2 \varphi \conj \varphi + {\conj \varphi}^2) \\
  &= 2 \varphi \conj \varphi
\end{align*}

Similarly, we also have:

\[
  \pdv{\varphi_1}{x_\mu} \pdv{\varphi_1}{x_\mu} + \pdv{\varphi_2}{x_\mu} \pdv{\varphi_2}{x_\mu} = 2 \pdv{\varphi}{x_\mu} \pdv{\conj \varphi}{x_\mu}
\]

Giving for the Lagrangian Density:

\begin{align*}
  \lagden_0 &= - \frac1{2} \qty(\pdv{\varphi_1}{x_\mu} \pdv{\varphi_1}{x_\mu} + \pdv{\varphi_2}{x_\mu} \pdv{\varphi_2}{x_\mu}) - \frac{m^2}{2} \qty(\varphi_1^2 + \varphi_2^2) \\
  &= - \frac1{2} \qty(2 \pdv{\varphi}{x_\mu} \pdv{\conj \varphi}{x_\mu}) - \frac{m^2}{2} \qty(2 \varphi \conj \varphi) \\
  &= - \pdv{\varphi}{x_\mu} \pdv{\conj \varphi}{x_\mu} - m^2 \varphi \conj \varphi
\end{align*}

Finally, the Euler-Lagrange equations can be determined.

\begin{align*}
  \pdv{\lagden_0}{\varphi} - \pdv{x_\mu}(\pdv{\lagden_0}{\pdv{\varphi}{x_\mu}}) &= 0 \\
  \pdv{\lagden_0}{\conj \varphi} - \pdv{x_\mu}(\pdv{\lagden_0}{\pdv{\conj \varphi}{x_\mu}}) &= 0
\end{align*}

Find the term by term derivatives:

\begin{align*}
  \pdv{\lagden_0}{\varphi} &= -m^2 \conj \varphi \\
  \pdv{\lagden_0}{\conj \varphi} &= -m^2 \varphi \\
  \pdv{\lagden_0}{\pdv{\varphi}{x_\mu}} &= - \pdv{\conj \varphi}{x_\mu} \\
  \pdv{\lagden_0}{\pdv{\conj \varphi}{x_\mu}} &= - \pdv{\varphi}{x_\mu} \\
\end{align*}

And finally state the Euler-Lagrange equation in $\varphi$ and in $\conj \varphi$:

\begin{align*}
  \pdv{\lagden_0}{\varphi} - \pdv{x_\mu}(\pdv{\lagden_0}{\pdv{\varphi}{x_\mu}}) &= 0 \\
  -m^2 \conj \varphi - \pdv{x_\mu}(- \pdv{\conj \varphi}{x_\mu}) &= 0 \\
  (\dalembertian - m^2) \conj \varphi &= 0 \\
  \pdv{\lagden_0}{\conj \varphi} - \pdv{x_\mu}(\pdv{\lagden_0}{\pdv{\conj \varphi}{x_\mu}}) &= 0 \\
  -m^2 \varphi - \pdv{x_\mu}(- \pdv{\varphi}{x_\mu}) &= 0 \\
  (\dalembertian - m^2) \varphi &= 0 \\
\end{align*}
