\prompt{The Lagrangian density $\lagden$, which generates a given set of
Euler-Lagrange equations, is not unique.  Prove this result by showing that
adding a divergence to $\lagden$ does not alter the Euler-Lagrange equations.
That is, let}

\[
  \lagden' = \lagden + \sum_k \pdv{f_k}{x_k}
\]

\prompt{where}

\begin{equation*}
  \begin{gathered}
    \lagden = \lagden\qty(x_k, \varphi_j, \pdv{\varphi_j}{x_k}); \\
    f_k = f_k(\varphi_j); \\
    \begin{aligned}
      j &= 1, \ldots, m && \qq{\prompt{indexes the dependent field variables, and}} \\
      k &= 1, \ldots, n && \qq{\prompt{indexes the independent variables.}}
    \end{aligned}
  \end{gathered}
\end{equation*}

\prompt{Now prove that}

\[
  \fdv{\lagden'}{\varphi_j} = \fdv{\lagden}{\varphi_j},
\]

\prompt{where we define}

\[
  \fdv{\lagden}{\varphi_j} = \pdv{\lagden}{\varphi_j} - \sum_i \pdv{x_i} \pdv{\lagden}{\pdv{\varphi_j}{x_i}}.
\]

First, let's state the value of $\lagden'$ a little more explicitly via the chain rule:

\[
  \lagden' = \lagden + \sum_{j, k} \pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k} \\
\]

Now let's begin by finding the various derivatives of interest.  Our functional
derivative involves the partial derivatives of the Lagrangian density with
respect to $\varphi_j$ and its derivatives.  To begin, let's investigate
$\pdv*{\lagden'}{\varphi_j}$.

\begin{align*}
  \pdv{\lagden'}{\varphi_j} &= \pdv{\lagden}{\varphi_j} + \pdv{\varphi_j}(\sum_{j,k} \pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k}) \\
  \pdv{\lagden'}{\varphi_j} &= \pdv{\lagden}{\varphi_j} + \sum_{j,k} \pdv{\varphi_j}(\pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k}) \\
  \pdv{\varphi_j}(\pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k}) &= \pdv{\varphi_j}(\pdv{f_k}{\varphi_j}) \pdv{\varphi_j}{x_k} + \pdv{f_k}{\varphi_j} \pdv{\varphi_j}(\pdv{\varphi_j}{x_k}) \\
  &= \pdv[2]{f_k}{\varphi_j} \pdv{\varphi_j}{x_k} + \pdv{f_k}{\varphi_j} \pdv{\varphi_j}(\pdv{\varphi_j}{x_k}) \\
  &= \pdv{f_k}{\varphi_j}{x_k} \\
  \pdv{\lagden'}{\varphi_j} &= \pdv{\lagden}{\varphi_j} + \sum_{j,k} \pdv{f_k}{\varphi_j}{x_k}
\end{align*}

And next for its derivative in $x_i$ (careful of indexing!):

\begin{align*}
  \pdv{\lagden'}{\pdv{\varphi_j}{x_i}} &= \pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \pdv{\pdv{\varphi_j}{x_i}}(\sum_{j,k} \pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k}) \\
  \pdv{\lagden'}{\pdv{\varphi_j}{x_i}} &= \pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \sum_{j,k} \pdv{\pdv{\varphi_j}{x_i}}(\pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k}) \\
  \pdv{\pdv{\varphi_j}{x_i}}(\pdv{f_k}{\varphi_j} \pdv{\varphi_j}{x_k}) &= \pdv{\pdv{\varphi_j}{x_i}}(\pdv{f_k}{\varphi_j}) \pdv{\varphi_j}{x_k} + \pdv{f_k}{\varphi_j} \pdv{\pdv{\varphi_j}{x_i}}(\pdv{\varphi_j}{x_k}) \\
  &= \pdv{\varphi_j}{x_k} \pdv{\pdv{\varphi_j}{x_i}}(\pdv{f_k}{\varphi_j}) + \pdv{f_k}{\varphi_j} \pdv{\pdv{\varphi_j}{x_i}}(\pdv{\varphi_j}{x_k}) \\
  &= \lkdelta{ik} \pdv{f_k}{\varphi_j} \\
  \pdv{\lagden'}{\pdv{\varphi_j}{x_i}} &= \pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \sum_{j,k} \lkdelta{ik} \pdv{f_k}{\varphi_j} \\
  &= \pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \sum_j \pdv{f_i}{\varphi_j} \\
\end{align*}

And then finally, putting it all together:

\begin{align*}
  \fdv{\lagden'}{\varphi_j} &= \pdv{\lagden'}{\varphi_j} - \sum_i \pdv{x_i} \pdv{\lagden'}{\pdv{\varphi_j}{x_i}} \\
  &= \pdv{\lagden}{\varphi_j} + \sum_{j,k} \pdv{f_k}{\varphi_j}{x_k} - \sum_i \pdv{x_i}(\pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \sum_j \pdv{f_i}{\varphi_j}) \\
  &= \pdv{\lagden}{\varphi_j} - \sum_i \pdv{x_i} \pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \sum_{j,k} \pdv{f_k}{\varphi_j}{x_k} - \sum_i \pdv{x_i}(\sum_j \pdv{f_i}{\varphi_j}) \\
  &= \pdv{\lagden}{\varphi_j} - \sum_i \pdv{x_i} \pdv{\lagden}{\pdv{\varphi_j}{x_i}} + \sum_{j,k} \pdv{f_k}{\varphi_j}{x_k} - \sum_{i,j} \pdv{f_i}{\varphi_j}{x_i} \\
  &= \pdv{\lagden}{\varphi_j} - \sum_i \pdv{x_i} \pdv{\lagden}{\pdv{\varphi_j}{x_i}} \\
  \fdv{\lagden'}{\varphi_j} &= \fdv{\lagden}{\varphi_j}
\end{align*}
