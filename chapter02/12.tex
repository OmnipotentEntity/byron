\prompt{A Lagrangian density for classical electrodynamics is}

\[
  \lagden = - \frac1{16\pi} \sum_{i,j} F_{ij} F_{ij} + \frac1{c} \sum_i J_i A_i
\]

\prompt{with}

\[
  F_{ij} = \pdv{A_j}{x_i} - \pdv{A_i}{x_j}.
\]

\prompt{The four-vectors $\{A_i\}$ and $\{J_i\}$ are defined by}

\begin{align*}
  \{A_i\} &\equiv \{A_1, A_2, A_3, i\varphi\} \equiv \{\vec{A}, i\varphi\}, \\
  \{J_i\} &\equiv \{j_1, j_2, j_3, ic\rho\} \equiv \{\vec{j}, ic\rho\},
\end{align*}

\prompt{where $\vec{A}$ is the vector potential $(\vec{H} = \curl{\vec{A}})$, $\varphi$ is the scalar potential}

\[
  \qty(\vec{E} = - \grad{\varphi} - \frac1{c} \pdv{\vec{A}}{t}),
\]

\prompt{$\vec{j}$ is the current vector, and $\rho$ is the charge density (see
Problem~\ref{sec:01-13}).  Show that this Lagrangian density can be written in a
more conventional form as}

\[
  \lagden = \frac1{8\pi} (\vec{E}^2 - \vec{H}^2) + \frac1{c} \vec{j} \vdot \vec{A} - \rho \varphi.
\]

\prompt{Using Lagrange's equations with $\vec{A}$ and $\varphi$ as dependent
variables, show that this Lagrangian density leads to the familiar Maxwell
equations.}

We will be using the indexing convention that Roman letters ($i$, $j$, $k$) will
run from 1 to 3 and Greek letters ($\alpha$, $\beta$, $\gamma$), will run from 1
to 4.

Recall from Problem~\ref{sec:01-13} parts a-e.

\[
  \mat{F}_{\alpha \beta} = \begin{bmatrix}
    0 & H_z & -H_y & -iE_x \\
    -H_z & 0 & H_x & -iE_y \\
    H_y & -H_x & 0 & -iE_z \\
    iE_x & iE_y & iE_z & 0 \\
  \end{bmatrix}
\]

This fact can be determined from the definition of the curl and from the scalar
potential equation.  See the referenced derivation for details.

Calculating the sum involving $\mat{F}_{\alpha \beta}$ gives

\begin{align*}
  \mat{F}_{\alpha \beta} \mat{F}_{\alpha \beta} &= 
    \begin{aligned}
      & H_z^2 + {(-H_y)}^2 + {(-iE_x)}^2 + \\
      & {(-H_z)}^2 + H_z^2 + {(-iE_y)}^2 + \\
      & H_y^2 + {(-H_z)}^2 + {(-iE_z)}^2 + \\
      & {(iE_x)}^2 + {(iE_y)}^2 + {(iE_z)}^2
    \end{aligned} \\
  &= 2\qty(H_x^2 + H_y^2 + H_z^2 - E_x^2 - E_y^2 - E_z^2) \\
  &= 2\qty(\vec{H}^2 - \vec{E}^2).
\end{align*}

While the second sum gives

\begin{align*}
  J_\alpha A_\alpha &= \vec{j} \vdot \vec{A} + ic\rho \cdot i\varphi \\
  &= \vec{j} \vdot \vec{A} - c\rho\varphi. \\
\end{align*}

So putting it all together:

\begin{align*}
  \lagden &= - \frac1{16\pi} \mat{F}_{\alpha \beta} \mat{F}_{\alpha \beta} + \frac1{c} J_\alpha A_\alpha \\
  &= - \frac1{16\pi} \qty(2\qty(\vec{H}^2 - \vec{E}^2)) + \frac1{c} \qty(\vec{j} \vdot \vec{A} - c\rho\varphi) \\
  &= \frac1{8\pi} \qty(\vec{E}^2 - \vec{H}^2) + \frac1{c} \qty(\vec{j} \vdot \vec{A}) - \rho\varphi.
\end{align*}

Which is what we were seeking to prove.

Now to demonstrate that the Lagrangian density results in Maxwell's equations.

We will be working with the original form of the Lagrangian density, as this
helps simplify computation.

As a reminder, our goal are Maxwell's equations:

\begin{align*}
  \div{\vec{E}} &= 4 \pi \rho \\
  \div{\vec{H}} &= 0 \\
  \curl{\vec{E}} &= - \frac1{c} \pdv{\vec{H}}{t} \\
  \curl{\vec{H}} &= \frac1{c} \qty(4 \pi \vec{J} + \pdv{\vec{E}}{t})
\end{align*}

We get the second one for free, because $\vec{H} = \curl{\vec{A}}$.

Writing down the terms in index notation of each of the remaining three we get:

\begin{align*}
  \div{\vec{E}} &= \pd{i} E_i \\
  &= \pd{i} (- \pd{i} \varphi - \frac1{c} \pd{t} A_i) \\
  &= \pd{i} \qty(- \qty(-i \cdot i) \pd{i} \varphi - \frac{i}{ic} \pd{t} A_i) \\
  &= i \pd{i} \qty(\pd{i} A_4 - \pd{4} A_i) \\
  &= i \qty(\pd{i}^2 A_4 - \pd{i} \pd{4} A_i) \\
  {(\curl{\vec{E}})}_i &= \llevic{ijk} \pd{j} E_k \\
  &= \llevic{ijk} \pd{j} \qty(- \pd{k} \varphi - \frac1{c} \pd{t} A_k) \\
  &= \llevic{ijk} \qty(- \qty(-i \cdot i) \pd{j} \pd{k} \varphi - \frac{i}{ic} \pd{j} \pd{t} A_k) \\
  &= i \llevic{ijk} \qty(\pd{j} \pd{k} A_4 - \pd{j} \pd{4} A_k) \\
  {(\curl{\vec{H}})}_i &= \llevic{ijk} \pd{j} H_k \\
  &= \llevic{ijk} \pd{j} \llevic{klm} \pd{l} A_m \\
  &= \llevic{ijk} \llevic{klm} \pd{j} \pd{l} A_m \\
  &= (\lkdelta{il} \lkdelta{jm} - \lkdelta{im} \lkdelta{jl}) \pd{j} \pd{l} A_m \\
  &= \pd{j} \pd{i} A_j - \pd{j} \pd{j} A_i
\end{align*}

In the next set of derivations, we begin using the 4-vector forms.  Recall that $x_4 =
ict$.

\begin{align*}
  4 \pi \rho &= \frac{-i}{c} 4 \pi i c \rho \\
  &= -\frac{4 \pi i}{c} J_4 \\
  -\frac1{c} \pdv{\vec{H}}{t} &= - \frac{i}{ic} \pdv{\vec{H}}{t} \\
  &= -i \pdv{H}{x_4} \\
  &= -i \pd{4} \qty(\curl{A}) \\
  &= -i \pd{4} \llevic{ijk} \pd{j} A_k \\
  &= -i \llevic{ijk} \pd{4} \pd{j} A_k \\
  4 \pi \vec{J} &= 4 \pi J_i \\
  \frac1{c} \pdv{\vec{E}}{t} &= \frac{i}{ic} \pdv{\vec{E}}{t} \\
  &= i \pdv{\vec{E}}{x_4} \\
  &= i \pd{4} E_i \\
  &= i \pd{4} \qty(- \pd{i} \varphi - \frac1{c} \pd{t} A_i) \\
  &= i \pd{4} \qty(i \pd{i} A_4 - \frac{i}{ic} \pd{t} A_i) \\
  &= i \pd{4} \qty(i \pd{i} A_4 - i \pd{4} A_i) \\
  &= - \pd{4} \pd{i} A_4 + \pd{4}^2 A_i
\end{align*}

This gives us the following goal formula:

\begin{align*}
  \pd{i}^2 A_4 - \pd{i} \pd{4} A_i &= -4 \pi i J_4 \\
  \llevic{ijk} \qty(\pd{j} \pd{k} A_4 - \pd{j} \pd{4} A_k + \pd{4} \pd{j} A_k) &= 0 \\
  c \qty(\pd{j} \pd{i} A_j - \pd{j}^2 A_i) &= 4 \pi J_i - \pd{4} \pd{i} A_4 + \pd{4}^2 A_i \\
\end{align*}

Next, recall the Euler-Lagrange formula:

\[
  \pdv{\lagden}{A_\beta} - \pdv{x_\alpha}(\pdv{\lagden}{\pdv{A_\beta}{x_\alpha}}) = 0
\]

And our density:

\[
  \lagden = - \frac1{16\pi} F_{\alpha \beta} F_{\alpha \beta} + \frac1{c} J_\alpha A_\alpha
\]

Because:

\[
  F_{\alpha \beta} = \pdv{A_\beta}{x_\alpha} - \pdv{A_\alpha}{x_\beta}
\]

it is possible to rewrite the density as:

\begin{align*}
  \lagden &= - \frac1{16\pi} {\qty(\pdv{A_\beta}{x_\alpha} - \pdv{A_\alpha}{x_\beta})}^2 + \frac1{c} J_\alpha A_\alpha \\
  &= - \frac1{16\pi} \qty({\qty(\pdv{A_\alpha}{x_\beta})}^2 - 2 \pdv{A_\alpha}{x_\beta} \pdv{A_\beta}{x_\alpha} + {\qty(\pdv{A_\beta}{x_\alpha})}^2 ) + \frac1{c} J_\alpha A_\alpha \\
  &= - \frac1{16\pi} \qty(2 \pdv{A_\alpha}{x_\beta} \pdv{A_\alpha}{x_\beta} - 2 \pdv{A_\alpha}{x_\beta} \pdv{A_\beta}{x_\alpha}) + \frac1{c} J_\alpha A_\alpha
\end{align*}

One of the terms is easy.

\[
  \pdv{\lagden}{A_\beta} = \frac1{c} J_\beta
\]

The other term you need to be a bit more careful of, due to the summation.

\begin{align*}
  \pdv{\lagden}{\pdv{A_\beta}{x_\alpha}} &= \pdv{\pdv{A_\beta}{x_\alpha}}(- \frac1{16\pi} \qty(2 \pdv{A_\mu}{x_\nu} \pdv{A_\mu}{x_\nu} - 2 \pdv{A_\mu}{x_\nu} \pdv{A_\nu}{x_\mu}) + \frac1{c} J_\mu A_\mu) \\
  &= - \frac1{16\pi} \pdv{\pdv{A_\beta}{x_\alpha}}(2 \pdv{A_\mu}{x_\nu} \pdv{A_\mu}{x_\nu} - 2 \pdv{A_\mu}{x_\nu} \pdv{A_\nu}{x_\mu}) \\
  &= - \frac1{16\pi} \qty(4 \lkdelta{\beta\mu}\lkdelta{\alpha\nu} \pdv{A_\mu}{x_\nu} - 2 \qty(\lkdelta{\beta\mu}\lkdelta{\alpha\nu} \pdv{A_\nu}{x_\mu} + \lkdelta{\alpha\mu}\lkdelta{\beta\nu} \pdv{A_\mu}{x_\nu})) \\
  &= - \frac1{4\pi} \qty(\pdv{A_\beta}{x_\alpha} - \pdv{A_\alpha}{x_\beta})
\end{align*}

So putting it together:

\begin{align*}
  \pdv{\lagden}{A_\beta} &= \pdv{x_\alpha}(\pdv{\lagden}{\pdv{A_\beta}{x_\alpha}}) \\
  \frac1{c} J_\beta &= -\frac1{4\pi} \pdv{x_\alpha}(\pdv{A_\beta}{x_\alpha} - \pdv{A_\alpha}{x_\beta}) \\
  \frac{4\pi}{c} J_\beta &= \pd{\alpha} \qty(\pd{\beta} A_\alpha - \pd{\alpha} A_\beta) \\
  4\pi J_\beta &= c \qty(\pd{\alpha} \pd{\beta} A_\alpha - \pd{\alpha}^2 A_\beta) \\
\end{align*}

Chosing $\beta = 4$ and $\alpha = i$, we get the first goal formula.  Choosing $\beta = i$ and $\alpha = \alpha$ we get the third goal formula.  Finally, choosing $\beta \ne \alpha$ we get the second goal formula.  That satisfies all 16 PDEs and all 4 of Maxwell's equations.
