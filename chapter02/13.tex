\prompt{Prove that a particle moving under gravity in a plane from a fixed point
$P$ to a vertical line $L$ will reach the line in minimum time by following the
cycloid from $P$ to $L$ that intersects $L$ at right angles.}

Without loss of generality, we can prescribe that the starting point $P$ is the
origin, and $L$ can be chosen to be the line $x = x_2$, with $x_2$ as a
constant.

Recall that that solution to the brachistochrone is the parametric equation:

\begin{align*}
  x &= a(\theta - \sin \theta) \\
  y &= a(1 - \cos \theta)
\end{align*}

If the cycloid intersects the line at a right angle, then it follows that
$\dv{y}{\theta} = 0$ and $\dv{x}{\theta} \ne 0$ at the point of intersection.

\begin{align*}
  \dv{y}{\theta} = 0 &= a \sin(\theta) \\
  \theta &= n\pi : n \in \bZ \\
  \dv{x}{\theta} \ne 0 &= a(1 - \cos \theta) \\
  \theta &\ne 2n\pi : n \in \bZ
\end{align*}

Because only the first solution could possibly be the curve of fastest descent,
the value of $\theta$ must be $\pi$.

From the geometry of the cycloid, we can recognize $a$ as the radius $R$ of the
circle being rotated.  On the other hand, finding the time is somewhat more
tricky.

The time for the particle to move on the cycloid from point 1 to point 2 is
given by:

\begin{align*}
  \Delta t_{12} &= t_2 - t_1 \\
  &= \frac1{2g} \pint{x_1}{x_2}{\sqrt{\frac{1 + y'^2}{y}}}{x} \\
  \sqrt{\frac{1 + y'^2}{y}} \dd{x} &= \frac1{y} \sqrt{y(1 + y'^2)} \dd{x} \\
  &= \frac{\sqrt{2R}}{R(1- \cos \theta)} (R(1 - \cos \theta) \dd{\theta}) \\
  &= \sqrt{2R} \dd{\theta} \\
  \Delta t_{12} &= \frac1{2g} \pint{\theta_1}{\theta_2}{\sqrt{2R}}{\theta} \\
  &= \sqrt{\frac{R}{g}} \pint{\theta_1}{\theta_2}{}{\theta} \\
  &= \sqrt{\frac{R}{g}} (\theta_2 - \theta_1) \\
\end{align*}

Because $\theta_1 = 0$:

\[
  t = \sqrt{\frac{R}{g}} \theta
\]

We can use this to find the following parametric equations explictly in $R$ and
$t$.

\begin{align*}
  x &= R\qty(\sqrt{\frac{g}{R}} t - \sin \qty(\sqrt{\frac{g}{R}} t)) \\
  y &= R\qty(1 - \cos \qty(\sqrt{\frac{g}{R}} t))
\end{align*}

If we define $\omega \equiv \sqrt{\frac{g}{R}}$ then we get:

\begin{align*}
  x &= R\qty(\omega t - \sin \qty(\omega t)) \\
  y &= R\qty(1 - \cos \qty(\omega t))
\end{align*}

We want to optimize for time, subject to the constraint that at the time the $x$
position is exactly $x_2$.  So we can set up the following Lagrangian.

\begin{align*}
  L(R, t, \lambda) &= t - \lambda \qty(R \qty(\omega t - \sin \qty(\omega t) - x_2)) \\
  \grad L = 0 &= 
    \begin{bmatrix}
      -\lambda \qty(\omega t - \sin \omega t + \omega' R t (1 - \cos \omega t)) \\
      1 - R \lambda \omega \qty(1 - \cos \qty(\omega t)) \\
      R \qty(\omega t - \sin \qty(\omega t)) - x_2
    \end{bmatrix}
\end{align*}

Where $\omega' = \dv{\omega}{R} = -\frac{\omega}{2R}$. This is frankly a bit of a mess.  However, if
we replace $\omega t = \pi$ (which is what we are seeking to prove), then we
get: 

\begin{align*}
  \grad L = 0 &= 
    \begin{bmatrix}
      -\lambda \qty(\omega t - \sin \omega t + \omega' R t (1 - \cos \omega t)) \\
      1 - R \lambda \omega \qty(1 - \cos \qty(\omega t)) \\
      R \qty(\omega t - \sin \qty(\omega t)) - x_2
    \end{bmatrix} \\
    0 &= -\lambda \qty(\pi - \sin \pi - \frac{\pi}{2} (1 - \cos \pi)) \\
    &= -\lambda \qty(\pi - 0 - \pi) = 0 \\
    0 &= 1 - R \lambda \omega \qty(1 - \cos \qty(\pi)) \\
    1 &= 2R \lambda \omega \\
    0 &= R \qty(\pi - 0) - x_2 \\
    x_2 &= R \pi \\
    1 &= 2R \lambda \omega \\
    1 &= 2\sqrt{gR} \lambda \\
    R &= \frac1{4 \lambda^2 g} \\
    x_2 &= \frac{\pi}{4 \lambda^2 g} \\
\end{align*}

Because the first equation gives a vacuous truth and this last line is
independent of $R$ and $t$, the identity holds for any choice of $R$ and $t$ and
is therefore a valid extremum.  Physically, this extremum must be a minimum. The
last statement gives a method of determining $R$ and $t$ from the constants
$x_2$ and $g$.  First you find $\lambda$, then using $\lambda$ you solve for $R$
and $t$.  Therefore, $\theta = \pi$ gives the minimum action, which proves the
statement.
