\prompt{Show that the eigenvalues of}

\[
  \mat{M} = \mqty[
    3 & 5 & 8 \\
    -6 & -10 & -16 \\
    4 & 7 & 11
  ]
\]

\prompt{are $\lambda = 0, 1, 3$, and that the corresponding eigenvectors are}

\[
  \mqty[1 \\ 1 \\ -1], \quad 
  \mqty[1 \\ -2 \\ 1], \quad
  \text{and} \quad
  \mqty[4 \\ -8 \\ 5].
\]

\prompt{Construct a diagonalizing matrix $\mat{P}$, prove that its inverse
exists, compute its inverse, and verify that $\inv{\mat{P}}\mat{MP}$ is diagonal
with the eigenvalues on the diagonal.  Note that $\det \mat{M} = 0$. Is it true
that any diagonalizable matrix with an eigenvalue equal to zero is singular?}

Proving this by hand is somewhat lengthy.  Let's begin.

First we find the eigenvalues of $\mat{M}$.

\begin{align*}
  \det(\mat{M} - \lambda \mat{I}) &= 0 \\
  \det(\mat{M} - \lambda \mat{I}) &= \mqty[
      3-\lambda & 5 & 8 \\
      -6 & -10-\lambda & -16 \\
      4 & 7 & 11-\lambda
    ] \\
  &= (3 - \lambda)((-10 - \lambda)(11 - \lambda) - (7 \cdot -16)) \\
  &- 5(-6 \cdot (11 - \lambda) - (4 \cdot -16)) \\
  &+ 8(-6 \cdot 7 - ((-10 - \lambda) \cdot 4)) \\
  &= (3 - \lambda)(\lambda^2 - \lambda - 110 + 112) \\
  &- 5(-66 + 6\lambda + 64) \\
  &+ 8(-42 + 40 + 4\lambda) \\
  &= 3\lambda^2 - 3 \lambda + 6 - \lambda^3 + \lambda^2 - 30 \lambda + 10 - 16 + 32 \lambda \\
  &= -\lambda^3 + 4 \lambda^2 - 3\lambda = 0 \\
  0 &= \lambda(\lambda - 1)(\lambda - 3) \\
  \lambda &= 0 \: \text{or} \: 1 \: \text{or} \: 3
\end{align*}

Using the eigenvalues we can find the eigenvectors.  The approach we will be
using is using matrix row elimination to solve the system of equations.

First, for $\lambda = 0$, 

\begin{align*}
  \mat{M} - \lambda \mat{I} &= \mqty[
      3-\lambda & 5 & 8 \\
      -6 & -10-\lambda & -16 \\
      4 & 7 & 11-\lambda
    ] \vec{x} = 0 \\
  &\implies \mqty[
      3 & 5 & 8 \\
      -6 & -10 & -16 \\
      4 & 7 & 11
    ] \\
  &\implies \mqty[
      4 & 7 & 11 \\
      3 & 5 & 8 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      12 & 21 & 33 \\
      12 & 20 & 32 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      12 & 21 & 33 \\
      0 & 1 & 1 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      4 & 7 & 11 \\
      0 & 1 & 1 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      4 & 0 & 4 \\
      0 & 1 & 1 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      1 & 0 & 1 \\
      0 & 1 & 1 \\
      0 & 0 & 0 \\
  ] \\
  x_1 &= -x_3 \\
  x_2 &= -x_3 \\
\end{align*}

So an eigenvector that satisfies these two constraints is:

\[
  \mqty[1 \\ 1 \\ -1].
\]

Second, for $\lambda = 1$,

\begin{align*}
  \mat{M} - \lambda \mat{I} &= \mqty[
      3-\lambda & 5 & 8 \\
      -6 & -10-\lambda & -16 \\
      4 & 7 & 11-\lambda
    ] \vec{x} = 0 \\
  &\implies \mqty[
      2 & 5 & 8 \\
      -6 & -11 & -16 \\
      4 & 7 & 10
    ] \\
  &\implies \mqty[
      2 & 5 & 8 \\
      0 & 4 & 8 \\
      0 & -3 & -6 \\
  ] \\
  &\implies \mqty[
      2 & 5 & 8 \\
      0 & 1 & 2 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      2 & 0 & -2 \\
      0 & 1 & 2 \\
      0 & 0 & 0 \\
  ] \\
  &\implies \mqty[
      1 & 0 & -1 \\
      0 & 1 & 2 \\
      0 & 0 & 0 \\
  ] \\
  x_1 &= x_3 \\
  x_2 &= -2x_3 \\
\end{align*}

So an eigenvector that satisfies these two constraints is:

\[
  \mqty[1 \\ -2 \\ 1].
\]

Finally, for the third eigenvalue $\lambda = 3$,

\begin{align*}
  \mat{M} - \lambda \mat{I} &= \mqty[
      3-\lambda & 5 & 8 \\
      -6 & -10-\lambda & -16 \\
      4 & 7 & 11-\lambda
    ] \vec{x} = 0 \\
  &\implies \mqty[
      0 & 5 & 8 \\
      -6 & -13 & -16 \\
      4 & 7 & 8
    ] \\
  &\implies \mqty[
      12 & 21 & 24 \\
      12 & 26 & 32 \\
      0 & 5 & 8
  ] \\
  &\implies \mqty[
      4 & 7 & 8 \\
      0 & 5 & 8 \\
      0 & 5 & 8
  ] \\
  &\implies \mqty[
      4 & 7 & 8 \\
      0 & 5 & 8 \\
      0 & 0 & 0
  ] \\
  &\implies \mqty[
      4 & 2 & 0 \\
      0 & 5 & 8 \\
      0 & 0 & 0
  ] \\
  &\implies \mqty[
      2 & 1 & 0 \\
      0 & 5 & 8 \\
      0 & 0 & 0
  ] \\
  2x_1 &= -x_2 \\
  5x_2 &= -8x_3 \\
\end{align*}

An eigenvector that satisfies these two constraints is:

\[
  \mqty[4 \\ -8 \\ 5].
\]

Now that we have our $\mat{P}$.  The column vector matrix consisting of the
three eigenvectors, namely.

\[
  \mat{P} = \mqty[
    1 & 1 & 4 \\
    1 & -2 & -8 \\
    -1 & 1 & 5
  ]
\]

Now we must find $\inv{\mat{P}}$.  Recall that $\inv{\mat{A}} = \tfrac{\cadj
\mat{A}}{\det \mat{A}}$.  We can prove that the inverse exists if $\det \mat{A}
\ne 0$.  Because we have to calculate this anyway to find the inverse, it's not
really out of our way.

\begin{align*}
  \det \mat{P} &= 1 \cdot (-2 \cdot 5 - (-8 \cdot 1)) \\
  &- 1 \cdot (1 \cdot 5 - (-1 \cdot -8)) \\
  &+ 4 \cdot(1 \cdot 1 - (-2 \cdot -1)) \\
  &= (-10 + 8) - (5 - 8) + 4(1 - 2) \\
  &= -2 + 3 - 4 = -3
\end{align*}

The definition of the classical adjoint is:

\[
  (\cadj \mat{A})_{ij} = \cof(A_{ji})
\]

Where the cofactor is defined in the book as:

\[
  \cof(A_{Jj}) = (-1)^{J+j} \tensor{\tilde{\varepsilon}}{_{a \ldots i k \ldots n}} A_{1a} \cdots A_{Ii} A_{Kk} \cdots A_{Nn}
\]

(Where $\tilde{\varepsilon}$ was defined as the $\varepsilon$ with an index
missing.)

Using this definition, it is possible to calculate $\cadj \mat{\mat{A}}$.

\[
  \cadj \mat{\mat{A}} = \mqty[
    (-1)^{1 + 1} \det \mqty[
      -2 & -8 \\
      1 & 5
    ] &
    (-1)^{1 + 2} \det \mqty[
      1 & 4 \\
      1 & 5
    ] &
    (-1)^{1 + 3} \det \mqty[
      1 & 4 \\
      -2 &-8 
    ] \\
    (-1)^{2 + 1} \det \mqty[
      1 & -8 \\
      -1 & 5
    ] &
    (-1)^{2 + 2} \det \mqty[
      1 & 4 \\
      -1 & 5
    ] &
    (-1)^{2 + 3} \det \mqty[
      1 & 4 \\
      1 & -8
    ] \\
    (-1)^{3 + 1} \det \mqty[
      1 & -2 \\
      -1 & 1
    ] &
    (-1)^{3 + 2} \det \mqty[
      1 & 1 \\
      -1 & 1
    ] &
    (-1)^{3 + 3} \det \mqty[
      1 & 1 \\
      1 & -2
    ] \\
  ]
\]

Rather than calculating everything directly in the matrix, which would be more
than a little messy, we have broken out the calculations separately.

\begin{align*}
    (-1)^{1 + 1} \det \mqty[
      -2 & -8 \\
      1 & 5
    ] &= -10 + 8 = 2 \\
    (-1)^{1 + 2} \det \mqty[
      1 & 4 \\
      1 & 5
    ] &= -1 (5 - 4) = -1 \\
    (-1)^{1 + 3} \det \mqty[
      1 & 4 \\
      -2 &-8 
    ] &= -8 + 8 = 0 \\
    (-1)^{2 + 1} \det \mqty[
      1 & -8 \\
      -1 & 5
    ] &= -1 (5 - 8) = 3 \\
    (-1)^{2 + 2} \det \mqty[
      1 & 4 \\
      -1 & 5
    ] &= 5 + 4 = 9 \\
    (-1)^{2 + 3} \det \mqty[
      1 & 4 \\
      1 & -8
    ] &= -1(-8 - 4) = 12 \\
    (-1)^{3 + 1} \det \mqty[
      1 & -2 \\
      -1 & 1
    ] &= 1 - 2 = -1 \\
    (-1)^{3 + 2} \det \mqty[
      1 & 1 \\
      -1 & 1
    ] &= -1 (1 + 1) = -2 \\
    (-1)^{3 + 3} \det \mqty[
      1 & 1 \\
      1 & -2
    ] &= -2 -1 = -3
\end{align*}

So the classical adjoint matrix $\cadj \mat{P}$ is

\[
  \cadj \mat{P} = \mqty[
    -2 & -1 & 0 \\
    3 & 9 & 12 \\
    -1 & -2 & -3
  ].
\]

So the inverse matrix is

\[
  \inv{\mat{P}} = \frac{\cadj \mat{P}}{\det \mat{P}} = \mqty[
    \tfrac{2}{3} & \tfrac{1}{3} & 0 \\
    -1 & -3 & -4 \\
    \tfrac{1}{3} & \tfrac{2}{3} & \tfrac{3}{3}
  ]
\]

Whew.  Take a breather.

Now we must verify that $\inv{\mat{P}}\mat{MP}$ is diagonal.

\begin{align*}
  \inv{\mat{P}}\mat{M} &= \mqty[
    \tfrac{2}{3} & \tfrac{1}{3} & 0 \\
    -1 & -3 & -4 \\
    \tfrac{1}{3} & \tfrac{2}{3} & \tfrac{3}{3}
  ]
  \mqty[
    3 & 5 & 8 \\
    -6 & -10 & -16 \\
    4 & 7 & 11
  ] \\ \\
  &\begin{aligned}
    \tfrac{2}{3} \cdot 3 + \tfrac{1}{3} \cdot -6 + 0 \cdot 4 &= 0 \\
    \tfrac{2}{3} \cdot 5 + \tfrac{1}{3} \cdot -10 + 0 \cdot 7 &= 0 \\
    \tfrac{2}{3} \cdot 8 + \tfrac{1}{3} \cdot -16 + 0 \cdot 11 &= 0 \\
    \\
    -1 \cdot 3 + -3 \cdot -6 + -4 \cdot 4 &= -1 \\
    -1 \cdot 5 + -3 \cdot -10 + -4 \cdot 7 &= -3 \\
    -1 \cdot 8 + -3 \cdot -16 + -4 \cdot 11 &= -4 \\
    \\
    \tfrac{1}{3} \cdot 3 + \tfrac{2}{3} \cdot -6 + 1 \cdot 4 &= 1 \\
    \tfrac{1}{3} \cdot 5 + \tfrac{2}{3} \cdot -10 + 1 \cdot 7 &= 2 \\
    \tfrac{1}{3} \cdot 8 + \tfrac{2}{3} \cdot -16 + 1 \cdot 11 &= 3 \\
  \end{aligned} \\ \\
  \inv{\mat{P}}\mat{M} &= \mqty[
    0 & 0 & 0 \\
    -1 & -3 & -4 \\
    1 & 2 & 3
  ] \\
  \inv{\mat{P}}\mat{MP} &= \mqty[
    0 & 0 & 0 \\
    -1 & -3 & -4 \\
    1 & 2 & 3
  ]
  \mqty[
    1 & 1 & 4 \\
    1 & -2 & -8 \\
    -1 & 1 & 5
  ] \\ \\
  &\begin{aligned}
    0 \cdot 1 + 0 \cdot 1 + 0 \cdot -1 &= 0 \\
    0 \cdot 1 + 0 \cdot -2 + 0 \cdot 1 &= 0 \\
    0 \cdot 4 + 0 \cdot -8 + 0 \cdot 5 &= 0 \\
    \\
    -1 \cdot 1 - 3 \cdot 1 - 4 \cdot -1 &= 0 \\
    -1 \cdot 1 - 3 \cdot -2 - 4 \cdot 1 &= 1 \\
    -1 \cdot 4 - 3 \cdot -8 - 4 \cdot 5 &= 0 \\
    \\
    1 \cdot 1 + 2 \cdot 1 + 3 \cdot -1 &= 0 \\
    1 \cdot 1 + 2 \cdot -2 + 3 \cdot 1 &= 0 \\
    1 \cdot 4 + 2 \cdot -8 + 3 \cdot 5 &= 3 \\
  \end{aligned} \\ \\
  \inv{\mat{P}}\mat{MP} &= \mqty[\dmat[0]{0,1,3}]
\end{align*}

So now that we have (finally) worked through the diagonalization of this matrix.
We can answer the second part of the question.  It is true that any
diagonalizable matrix with an eigenvalue equal to zero is singular.  This is
because the eigenvalue expression $\det(\mat{M} - \lambda \mat{I}) = 0$ becomes
just $\det \mat{M} = 0$ when $\lambda = 0$.  This directly implies that the
matrix is singular, because its determinant is 0.
