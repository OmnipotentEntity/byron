\prompt{\emph{The Dirac matrices.}  Assume that for some $n$ there exist
\emph{four} $n \times n$ matrices which satisfy the anticommutation relations
$\mat{\gamma}_i \mat{\gamma}_j + \mat{\gamma}_j \mat{\gamma}_i = 2 \mat{I}
\lkdelta{ij}$, where $i, j =$ 1, 2, 3, or 4.}

\prompt{a) Prove that $\mat{\gamma}_i^2 = \mat{I}$ (the unit matrix), for $i = $
1, 2, 3, or 4.}

Given that $\mat{\gamma}_i \mat{\gamma}_j + \mat{\gamma}_j \mat{\gamma}_i = 2 \mat{I}
\lkdelta{ij}$, it follows immediately once you set $i = j$.

\begin{align*}
  \mat{\gamma}_i \mat{\gamma}_j + \mat{\gamma}_j \mat{\gamma}_i &= 2 \mat{I} \lkdelta{ij} \\
  \mat{\gamma}_i \mat{\gamma}_i + \mat{\gamma}_i \mat{\gamma}_i &= 2 \mat{I} \lkdelta{ii} \\
  2 \mat{\gamma}_i^2 &= 2 \mat{I} \\
  \mat{\gamma}_i^2 &= \mat{I}
\end{align*}

\prompt{b) Prove that $\det \mat{\gamma}_i = \pm 1$, for $i = $ 1, 2, 3, or 4.}

Recall that $\det(\mat{AB}) = \det \mat{A} \det \mat{B}$.

\begin{align*}
  \det \mat{I} &= 1 \\
  \det \mat{\gamma}_i^2 &= 1 \\
  {(\det \mat{\gamma}_i)}^2 &= 1 \\
  \det \mat{\gamma}_i &= \pm 1
\end{align*}

\prompt{c) If $\lambda$ is and eigenvalue of any one of the $\mat{\gamma}_i$,
show that $\lambda = \pm 1$.}

Let $\vec{v}$ be an eigenvector of $\mat{\gamma}_i$ with associated eigenvalue
$\lambda$.

\begin{align*}
  \mat{\gamma}_i \vec{v} &= \lambda \vec{v} \\
  \mat{\gamma}_i^2 \vec{v} &= \lambda^2 \vec{v} \\
  \mat{I} \vec{v} &= \lambda^2 \vec{v} \\
  \lambda^2 &= 1 \\
  \lambda &= \pm 1
\end{align*}

\prompt{d) Show that $\tr \mat{\gamma}_i = 0$, for $i = $ 1, 2, 3, or 4.}

In order to prove this, it is first required to prove that $\tr(\mat{AB}) =
\tr(\mat{BA})$.

\begin{align*}
  \tr(\mat{AB}) &= {(ab)}_{ii} \\
  &= a_{ij} b_{ji} \\
  &= b_{ji} a_{ij} \\
  &= {(ba)}_{jj} \\
  &= \tr(\mat{BA})
\end{align*}

with this in hand consider the trace of $\mat{\gamma}_i$.

\begin{align*}
  \tr \mat{\gamma}_i &= \tr(\mat{\gamma}_i (\mat{\gamma}_j \mat{\gamma}_j)) \qqtext{Because $\mat{\gamma}_j^2 = \mat{I}$} \\
  &= \tr((\mat{\gamma}_i \mat{\gamma}_j) \mat{\gamma}_j) \\
  &= \tr(-(\mat{\gamma}_j \mat{\gamma}_i) \mat{\gamma}_j) \qqtext{By the anticommutation relation} \\
  &= - \tr(\mat{\gamma}_j \mat{\gamma}_i \mat{\gamma}_j) \\
  &= - \tr((\mat{\gamma}_j) (\mat{\gamma}_i \mat{\gamma}_j)) \\
  &= - \tr((\mat{\gamma}_i \mat{\gamma}_j) (\mat{\gamma}_j)) \qqtext{By our lemma} \\
  &= - \tr(\mat{\gamma}_i \mat{\gamma}_j \mat{\gamma}_j) \\
  &= - \tr(\mat{\gamma}_i) \\
\end{align*}

So the trace of $\mat{\gamma}_i$ is its own negative, so it must be 0.

\prompt{The final part of this problem consists in show that there exists not
set of four $n \times n$ matrices of order less than 4 which satisfies the above
anticommutation relations.  You may, of course, do this in any way you like.
One way is to prove the statement via the following two steps.}

\prompt{e) Show that there exists no set of four $n \times n$ matrices of odd
order which satisfies the anticommutation relations.}

Recall Problem~\rprob{3}{19} where it was proven that the sum of the eigenvalues
is the trace.  Combining this with section c and d of this problem, it is
straightforward to see that no such odd matrix can exist.  As every matrix has
the same count of eigenvalues as its rank (if repetations are included).
Therefore, in order for the trace to be zero, the sum of eigenvalues must be 0,
and because each eigenvalue is either -1 or 1, this means for an order $n$
matrix there must be $n/2$ eigenvalues with value -1 and $n/2$ eigenvalues with
value 1.  Implying that $n$ must be even, because it is divisible by 2.

\prompt{f) Show that any \emph{four} $2 \times 2$ matrices cannot satisfy the
anticommutation relations.  [\emph{Hint}: The three $2 \times 2$ Pauli matrices
do satisfy the relations}

\[
  \mat{\sigma}_i \mat{\sigma}_j + \mat{\sigma}_j \mat{\sigma}_i = 2 \mat{I} \lkdelta{ij}, \qquad i, j = 1, 2, 3,
\]

\prompt{and any $2 \times 2$ matrices can be expanded as a linear combination of
the three Pauli matrices and the $2 \times 2$ unit matrix.]}

Consider an anticommutation relationship where one of the matrices is a linear
combination of the others, let us say that there are some values $a_i$ such that
$\mat{\gamma}_4 = a_i \mat{\gamma}_i$ where summation is implied over $i = 1, 2,
3$.  We know that

\[
  \mat{\gamma}_i \mat{\gamma}_4 + \mat{\gamma}_4 \mat{\gamma}_i = 0
\]

where $i \ne 4$ by the anticommutation relation.  However,

\begin{align*}
  0 &= \mat{\gamma}_i \mat{\gamma}_4 + \mat{\gamma}_4 \mat{\gamma}_i \\
  &= \mat{\gamma}_i (a_j \mat{\gamma}_j) + (a_j \mat{\gamma}_j) \mat{\gamma}_i \\
  &= a_j (\lkdelta{ij} \mat{I} + (1 - \lkdelta{ij}) \mat{\gamma}_i \mat{\gamma}_j) + \\
  &+ a_j (\lkdelta{ij} \mat{I} + (1 - \lkdelta{ij}) \mat{\gamma}_j \mat{\gamma}_i) \\
  &= 2 a_j \mat{I} \\
\end{align*}

Implying that the inequality only holds if $a_j = 0$ for any selection of $j$.
Therefore, $\mat{\gamma}_4 = 0$. However, that violates the other half of the
commutation relationship, which is $\mat{\gamma}_4^2 = \mat{I}$.  Therefore our
assumption that $\mat{\gamma}_4$ is a linear combination of the other matrices
is an impossible assumption, and all of the $\mat{\gamma}_i$ matrices must be
linearly independent.

Because it is possible to represent all $2 \times 2$ matrices as a linear
combination of the Pauli spin matrices and the unit matrix, then if there exists
a set of four $2 \times 2$ matrices that satisfy the anticommutation relation
then they too are a linear combination of these 4 basis matrices.

Because we have 4 basis matrices in our vector space, and we are seeking 4
linearly independent matrices, then we must be looking for another basis of this
vector space.  This means there exists a $4 \times 4$ transformation matrix
between the two bases.  Let us call this matrix $\mat{A}$, and let
${(\mat{A})}_{ij} = a_{ij}$.  And let $\mat{A}\vec{\mat{\sigma}} =
\vec{\mat{\gamma}}$, where $\vec{\mat{\sigma}}$ is the column vector containing
the $\mat{\sigma}_i$s and $\mat{I}$ (which will be $\mat{\sigma}_4$ for
simplicity) and $\vec{\mat{\gamma}}$ is the column vector containing the
$\mat{\gamma}_i$s.

It is somewhat strange to thing about a column vector containing matrices, and
a vector space whose basis is matrices.  However, it is a valid and powerful
lens to view this problem with.

Let us consider the equation $\mat{\gamma}_i \mat{\gamma}_i = \mat{\mat{I}}$.
From our basis transformation, we can rewrite this as

\begin{align*}
  \mat{\gamma}_i &= a_{ij} \mat{\sigma}_j \\
  \mat{\gamma}_i^2 &= (a_{ij} \mat{\sigma}_j) (a_{ik} \mat{\sigma}_k) \\
  \mat{I} &= a_{i1} a_{i1} \mat{\sigma}_1 \mat{\sigma}_1 + a_{i1} a_{i2} \mat{\sigma}_1 \mat{\sigma}_2 + a_{i1} a_{i3} \mat{\sigma}_1 \mat{\sigma}_3 + a_{i1} a_{i4} \mat{\sigma}_1 \mat{I} \\
  &+ a_{i2} a_{i1} \mat{\sigma}_2 \mat{\sigma}_1 + a_{i2} a_{i2} \mat{\sigma}_2 \mat{\sigma}_2 + a_{i2} a_{i3} \mat{\sigma}_2 \mat{\sigma}_3 + a_{i2} a_{i4} \mat{\sigma}_2 \mat{I} \\
  &+ a_{i3} a_{i1} \mat{\sigma}_3 \mat{\sigma}_1 + a_{i3} a_{i2} \mat{\sigma}_3 \mat{\sigma}_2 + a_{i3} a_{i3} \mat{\sigma}_3 \mat{\sigma}_3 + a_{i3} a_{i4} \mat{\sigma}_3 \mat{I} \\
  &+ a_{i4} a_{i1} \mat{I} \mat{\sigma}_1 + a_{i4} a_{i2} \mat{I} \mat{\sigma}_2 + a_{i4} a_{i3} \mat{I} \mat{\sigma}_3 + a_{i4} a_{i4} \mat{I} \mat{I} \\
  \mat{I} &= a_{i1}^2 \mat{I} + i a_{i1} a_{i2} \mat{\sigma}_3 - i a_{i1} a_{i3} \mat{\sigma}_2 + a_{i1} a_{i4} \mat{\sigma}_1 \\
  &- i a_{i2} a_{i1} \mat{\sigma}_3 + a_{i2}^2 \mat{I} + i a_{i2} a_{i3} \mat{\sigma}_1 + a_{i2} a_{i4} \mat{\sigma}_2 \\
  &+ i a_{i3} a_{i1} \mat{\sigma}_2 - i a_{i3} a_{i2} \mat{\sigma}_1 + a_{i3}^2 \mat{I} + a_{i3} a_{i4} \mat{\sigma}_3 \\
  &+ a_{i4} a_{i1} \mat{\sigma}_1 + a_{i4} a_{i2} \mat{\sigma}_2 + a_{i4} a_{i3} \mat{\sigma}_3 + a_{i4} a_{i4} \mat{I} \\
  \mat{I} &= a_{i1}^2 \mat{I} + a_{i1} a_{i4} \mat{\sigma}_1 \\
  &+ a_{i2}^2 \mat{I} + a_{i2} a_{i4} \mat{\sigma}_2 \\
  &+ a_{i3}^2 \mat{I} + a_{i3} a_{i4} \mat{\sigma}_3 \\
  &+ a_{i4} a_{i1} \mat{\sigma}_1 + a_{i4} a_{i2} \mat{\sigma}_2 + a_{i4} a_{i3} \mat{\sigma}_3 + a_{i4} a_{i4} \mat{I} \\
  \mat{I} &= (a_{i1}^2 + a_{i2}^2 + a_{i3}^2 + a_{i4}^2) \mat{I} + 2 a_{i1} a_{i4} \mat{\sigma}_1 + 2 a_{i2} a_{i4} \mat{\sigma}_2 + 2 a_{i3} a_{i4} \mat{\sigma}_3 \\
\end{align*}

From here we can build simultaneous equations in $i$, because $\mat{I} = 0 \mat{\sigma}_1 +
0 \mat{\sigma}_2 + 0 \mat{\sigma}_3 + 1 \mat{I}$:

\begin{align*}
  0 &= a_{i1} a_{i4} \\
  0 &= a_{i2} a_{i4} \\
  0 &= a_{i3} a_{i4} \\
  1 &= a_{i1}^2 + a_{i2}^2 + a_{i3}^2 + a_{i4}^2 \\
\end{align*}

From this we can quickly conclude that either $a_{i4} = 0$ or all of $a_{i1}$,
$a_{i2}$, and $a_{i3}$ are zero.  Either way, $i$ is a free variable, so this
implies either one row or three rows of the matrix $A$ are all 0.  Which implies
that $A$ is singular. However, this matrix was a basis transformation matrix,
which is not allowed to be singular.  This is a contradiction, therefore our
assumption is incorrect.  And there is no such set of four $2 \times 2$
matrices.
