\prompt{Prove that if $\mat{A}$ is self-adjoint, then $\det \mat{A}$ is real.
(This is all one can say about the determinant of a self-adjoint matrix, whereas
if $\mat{B}$ is unitary, then $| \det \mat{B} | = 1$.  Prove this also.)}

Recall that 

\[
  \det \mat{A} = \llevic{abc \ldots n} A_{1a} A_{2B} A_{3c} \cdots A_{Nn}
\]

Therefore,

\begin{align*}
  \conj{(\det \mat{A})} &= \conj{(\llevic{abc \ldots n} A_{1a} A_{2B} A_{3c} \cdots A_{Nn})} \\
  &= \conj{\llevic{abc \ldots n}} \conj{A_{1a}} \conj{A_{2B}} \conj{A_{3c}} \cdots \conj{A_{Nn}} \\
  \conj{\llevic{abc \ldots n}} &= \llevic{abc \ldots n} \\
  \conj{(\det \mat{A})} &= \llevic{abc \ldots n} \conj{A_{1a}} \conj{A_{2B}} \conj{A_{3c}} \cdots \conj{A_{Nn}} \\
  &= \det \conj{\mat{A}}
\end{align*}

Additionally, $\det \mat{A} = \det \transpose{\mat{A}}$, so given a
inner-product space over a complex field,

\begin{align*}
  \det \mat{A} &= \det \adj{\mat{A}} \\
  &= \det \transpose{\conj{\mat{A}}} \\
  &= \det \conj{\mat{A}} \\
  &= \conj{(\det \mat{A})}
\end{align*}

Because the complex conjugate of the determinant is equal to the determinant
itself, the value must be real.

Note that this proof was for a Hermitian matrix, rather than a self-adjoint
matrix.  Because a self-adjoint matrix is either Hermitian or symmetric
depending on the underlying field of the matrix, and because the complex
conjugation is a no-op under the reals, this proof is sufficient for the
symmetric matrix case as well.  This fact will be assumed for the remainder of
this document.

If $\mat{B}$ is unitary then $\adj{\mat{B}} \mat{B} = \mat{I}$, so

\begin{align*}
  \det \mat{I} &= \det (\adj{\mat{B}} \mat{B}) \\
  1 &= \det \adj{\mat{B}} \det \mat{B} \\
  &= \conj{(\det \mat{B})} \det \mat{B} \\
  | \det \mat{B} | &= 1 \\
\end{align*}

Which is what we wanted to prove.  Note, that $\det \mat{B}$ can be any complex
number with a norm of 1 if $\mat{B}$ is Hermitian.  However, if $\mat{B}$ is
symmetric, then $\det \mat{B}$ is only either -1 or 1.
