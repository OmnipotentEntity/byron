\prompt{Let $\mat{B}$ be an $n \times n$ matrix.  Define the $n \times n$ matrix
$\mat{A} = \E{i \mat{B}}$ in terms of the power series expansion of the
exponential.  That is,}

\[
  \mat{A} = \sum_{n=0}^{\infty} \frac{{(i\mat{B})}^n}{n!} = \mat{I} + i\mat{B} + \frac{{(i\mat{B})}^2}{2!} + \cdots
\]

\prompt{Assuming that the series converges, prove that $\mat{A}$ is isometric if
$\mat{B}$ is self-adjoint.  Also show that any isometry $\mat{U}$ can be written
as $\mat{U} = \exp(i \mat{H})$, where $\mat{H}$ is Hermitian.  Obtain an
expression for $\mat{H}$ in terms of quantities related to $\mat{U}$. [
\emph{Hint}: the properties of the eigenvectors and eigenvalues of the operators
will be helpful. ]}

If we define $\cos \mat{B}$ and $\sin \mat{B}$ using analogous series
definitions as their real valued counterparts, we can see that the same basic
properties with respect to their identities holds.  Specifically we will be
using:

\begin{align*}
  \E{i \mat{B}} &= \cos \mat{B} + i \sin \mat{B} \\
  \mat{I} &= \sin^2 \mat{B} + \cos^2 \mat{B} \\
\end{align*}

The first can be proven via inspection of the relevant series: every term with
an even $n$ goes to the cosine and every odd term goes to the sine.

The second is a bit more tricky to demonstrate.  We will be using the fact that
any matrix commutes with itself extensively. We will also be using the fact that
the series is absolutely convergent. 

\begin{align*}
  \cos^2 \mat{B} &= {\qty(\sum_{n=0}^{\infty} {(-1)}^n \frac{\mat{B}^{2n}}{(2n)!})}^2 \\
  &= \sum_{n=0}^{\infty} \sum_{k=0}^{n} {(-1)}^{n-k} \frac{\mat{B}^{2n-2k}}{(2n - 2k)!} {(-1)}^{k} \frac{\mat{B}^{2k}}{(2k)!} \\
  &= \sum_{n=0}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n}  \frac{{(-1)}^n}{(2n - 2k)!(2k)!} \\
  &= \mat{I} + \sum_{n=1}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n}  \frac{{(-1)}^n}{(2n - 2k)!(2k)!} \\
  \\
  \sin^2 \mat{B} &= {\qty(\sum_{n=0}^{\infty} {(-1)}^n \frac{\mat{B}^{2n + 1}}{(2n + 1)!})}^2 \\
  &= \sum_{n=0}^{\infty} \sum_{k=0}^{n} {(-1)}^{n-k} \frac{\mat{B}^{2n-2k+1}}{(2n - 2k + 1)!} {(-1)}^{k} \frac{\mat{B}^{2k + 1}}{(2k + 1)!} \\
  &= \sum_{n=0}^{\infty} \mat{B}^{2n + 2} \sum_{k=0}^{n}  \frac{{(-1)}^n}{(2n - 2k + 1)!(2k + 1)!} \\
  &= \sum_{n=1}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n-1}  \frac{{(-1)}^{n-1}}{(2n - 2k - 1)!(2k + 1)!} \\
\end{align*}

If our identity holds, then clearly the two sums must add to zero.

\begin{align*}
  \cos^2 \mat{B} + \sin^2 \mat{B} &= \mat{I} + \sum_{n=1}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n}  \frac{{(-1)}^n}{(2n - 2k)!(2k)!} \\
  &+ \sum_{n=1}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n-1}  \frac{{(-1)}^{n-1}}{(2n - 2k - 1)!(2k + 1)!} \\
  0 &\stackrel{?}{=} \sum_{n=1}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n}  \frac{{(-1)}^n}{(2n - 2k)!(2k)!} \\
  &+ \sum_{n=1}^{\infty} \mat{B}^{2n} \sum_{k=0}^{n-1}  \frac{{(-1)}^{n-1}}{(2n - 2k - 1)!(2k + 1)!} \\
  0 &\stackrel{?}{=} \sum_{k=0}^{n}  \frac{{(-1)}^n}{(2n - 2k)!(2k)!} + \sum_{k=0}^{n-1}  \frac{{(-1)}^{n-1}}{(2n - 2k - 1)!(2k + 1)!} \qq{For $n \ge{} 1$} \\
  0 &\stackrel{?}{=} \sum_{k=0}^{n}  \frac{1}{(2n - 2k)!(2k)!} - \sum_{k=0}^{n-1}  \frac{1}{(2n - 2k - 1)!(2k + 1)!} \\
  0 &\stackrel{?}{=} \sum_{k=0}^{2n} {(-1)}^k \frac{1}{(2n - k)! k!} \\
  0 &\stackrel{?}{=} \sum_{k=0}^{2n} \binom{2n}{k} {(-1)}^k \\
  0 &\stackrel{\checkmark}{=} {(1-1)}^{2n}
\end{align*}

So our basic trigonometric identity holds.  From here, the proof is
straightforward.

(It should be noted, that there are other, much simpler proofs that use specific
properties of the matrix exponential which have not yet been proven.  For
instance, one could just consider $\E{i \mat{B}} \E{-i \mat{B}} = \mat{I}$.
However, this would also require a summation proof to show formally, so I opted
to prove the identity directly.)

\begin{align*}
  \mat{A} &= \E{i \mat{B}} = \cos \mat{B} + i \sin \mat{B} \\
  \adj{\mat{A}} &= \adj{\E{i \mat{B}}} \\
  &= \adj{\qty(\mat{I} + i \mat{B} + \frac{{(i \mat{B})}^2}{2!} + \cdots)} \\
  &= \qty(\adj{\mat{I}} + \adj{i \mat{B}} + \frac{\adj{{(i \mat{B})}^2}}{2!} + \cdots) \\
  &= \qty(\mat{I} - i \mat{B} + \frac{{(-i \mat{B})}^2}{2!} + \cdots) \\
  &= \E{-i \mat{B}} = \cos \mat{B} - i \sin \mat{B} \\
  \mat{A}\adj{\mat{A}} &= (\cos \mat{B} + i \sin \mat{B})(\cos \mat{B} - i \sin \mat{B}) \\
  &= \cos^2 \mat{B} + (\cos \mat{B})(-i \sin \mat{B}) + (i \sin \mat{B})(\cos \mat{B}) + \sin^2 \mat{B} \\
  &= \cos^2 \mat{B} - i \cos \mat{B} \sin \mat{B} + i \sin \mat{B} \cos \mat{B} + \sin^2 \mat{B} \\
  &= \cos^2 \mat{B} - i \cos \mat{B} \sin \mat{B} + i \cos \mat{B} \sin \mat{B} + \sin^2 \mat{B} \\
  &= \cos^2 \mat{B} + \sin^2 \mat{B} = \mat{I}
\end{align*}

Therefore $\mat{A}$ is unitary.

To prove the second part let $\lambda$ and $\vec{v}$ be any eigenpair of
$\mat{B}$, then (because $\mat{B}$ is Hermitian) $\lambda$ is real, and
$\mat{B}\vec{v} = \lambda \vec{v}$.

\begin{align*}
  \mat{A} &= \mat{I} + i \mat{B} + \frac{{(i \mat{B})}^2}{2!} + \cdots \\
  \mat{A} \vec{v} &= \qty(\mat{I} + i \mat{B} + \frac{{(i \mat{B})}^2}{2!} + \cdots) \vec{v} \\
  \mat{A} \vec{v} &= \mat{I} \vec{v} + i \mat{B} \vec{v} + \frac{{(i \mat{B})}^2}{2!} \vec{v} + \cdots \\
  \mat{A} \vec{v} &= 1 \vec{v} + i \lambda \vec{v} + \frac{{(i\lambda)}^2}{2!} \vec{v} + \cdots \\
  \mat{A} \vec{v} &= \E{i\lambda} \vec{v} \\
\end{align*}

So if $\mat{A} = \E{i \mat{B}}$ then $\mat{A}$ and $\mat{B}$ have the same
eigenvectors.  And their eigenvalues are related by the transformation of a
complex exponential.  Due to Theorem 4.20, both $\mat{B}$ may be diagonalized
via a similarity transformation, and because $\mat{B}$ and $\mat{A}$ share
eigenvectors, $\mat{A}$ may be as well.  So considering the expression $\mat{U}
= \E{i \mat{H}}$, we have:

\begin{align*}
  \mat{H} &= \inv{\mat{P}} \mat{DP} \\
  \mat{U} &= \inv{\mat{P}} \E{i \mat{D}} \mat{P} \\
\end{align*}

Where $\E{i \mat{D}}$ is calculated by taking $\mat{D}$, the diagonal matrix of
$\mat{H}$'s eigenvalues, and for each entry $\lambda_i$ replace it with
$\E{i\lambda_i}$.  To go backwards and determine $\mat{H}$ from $\mat{U}$,
simply find the complex argument of each eigenvalue of $\mat{U}$, and that is
your eigenvalue for $\mat{H}$.  This determines the eigenvalue up to a multiple
of $2\pi k : k \in \bZ$.
