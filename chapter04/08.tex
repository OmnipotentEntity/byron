\prompt{In classical mechanics, the virial theorem states that the average value
of the potential energy of a simple harmonic oscillator is equal te average
value of its kinetic energy. If $\pip{\phi}{A\phi}$ denotes the average value in
the state $\phi$ of an observable $A$ (assuming that $\phi$ is normalized to
unity), show that this result is also true for the quantum-mechanical harmonic
oscillator.}

First, recall that the inner product in this vector space is continuous and
represented by:

\[
  \pip{\phi}{\psi} = \pint{-\infty}{\infty}{\conj{\phi}(x) \psi(x)}{x}
\]

Next, we know the explict ground state of the harmonic oscillator

\[
  \phi(x) = {\qty(\frac{m \omega}{\pi \hbar})}^{1/4} \E{-m \omega x^2/2 \hbar},
\]

and the Hamiltonian

\begin{align*}
  H &= \frac{p^2}{2m} + \frac1{2} k x^2 \\
  &= - \frac{\hbar^2}{2m} \dv[2]{x} + \frac1{2} m \omega^2 x^2.
\end{align*}

We can easily identify the components of the Hamiltonian which are potential and
kinetic energy from inspection.

\begin{align*}
  T &= - \frac{\hbar^2}{2m} \dv[2]{x} \\
  U &= \frac1{2} m \omega^2 x^2
\end{align*}

So our task is simply to show that $\pip{\phi}{T\phi} = \pip{\phi}{U\phi}$.

This is largely a mechanical process from here.  However, it is useful to note
the following identities, which may or may not already be familiar:

\begin{align*}
  \pint{-\infty}{\infty}{\E{-a x^2}}{x} &= \sqrt{\frac{\pi}{a}} \\
  \pint{-\infty}{\infty}{x^2 \E{-a x^2}}{x} &= \frac{\sqrt{\pi}}{2 a^{3/2}} \\
  \dv[2]{x}(\E{-a x^2}) &= (4a^2 x^2 - 2a) \E{-a x^2} \\
\end{align*}

Therefore,

\begin{align*}
  \pip{\phi}{T\phi} &= \pint{-\infty}{\infty}{\conj{\phi}(x) T \phi(x)}{x} \\
  &= \pint{-\infty}{\infty}{{\qty(\frac{m \omega}{\pi \hbar})}^{1/4} \E{-m \omega x^2/2 \hbar} \qty(- \frac{\hbar^2}{2m} \dv[2]{x}) \qty({\qty(\frac{m \omega}{\pi \hbar})}^{1/4} \E{-m \omega x^2/2 \hbar})}{x} \\
  &= - \frac{\hbar^2}{2m} \sqrt{\frac{m \omega}{\pi \hbar}} \pint{-\infty}{\infty}{\E{-m \omega x^2/2 \hbar} \dv[2]{x}(\E{-m \omega x^2/2 \hbar})}{x} \\
  &= - \frac{\hbar^2}{2m} \sqrt{\frac{m \omega}{\pi \hbar}} \pint{-\infty}{\infty}{\E{-m \omega x^2/2 \hbar} \qty(\frac{m^2 \omega^2}{\hbar^2} x^2 - \frac{m \omega}{\hbar})\qty(\E{-m \omega x^2/2 \hbar})}{x} \\
  &= - \frac{\hbar \omega}{2} \sqrt{\frac{m \omega}{\pi \hbar}} \pint{-\infty}{\infty}{\qty(\frac{m \omega}{\hbar} x^2 - 1) \E{-m \omega x^2/\hbar}}{x} \\
  &= - \frac{\hbar \omega}{2} \sqrt{\frac{m \omega}{\pi \hbar}} \qty(\qty(\frac{m \omega}{\hbar}) \frac{\sqrt{\pi}}{2 {\qty(\frac{m \omega}{\hbar})}^{3/2}} - \sqrt{\frac{\pi}{\frac{m \omega}{\hbar}}}) \\
  \pip{\phi}{T\phi} &= \frac{\hbar \omega}{4}, \\
  \pip{\phi}{U\phi} &= \pint{-\infty}{\infty}{\conj{\phi}(x) U \phi(x)}{x} \\
  &= \pint{-\infty}{\infty}{{\qty(\frac{m \omega}{\pi \hbar})}^{1/4} \E{-m \omega x^2/2 \hbar} \qty(\frac1{2} m \omega^2 x^2) \qty({\qty(\frac{m \omega}{\pi \hbar})}^{1/4} \E{-m \omega x^2/2 \hbar})}{x} \\
  &= \frac{m \omega^2}{2} \sqrt{\frac{m \omega}{\pi \hbar}} \pint{-\infty}{\infty}{x^2 \E{-m \omega x^2/\hbar}}{x} \\
  &= \frac{m \omega^2}{2} \sqrt{\frac{m \omega}{\pi \hbar}} \frac{\sqrt{\pi}}{2 {\qty(\frac{m \omega}{\hbar})}^{3/2}} \\
  \pip{\phi}{U\phi} &= \frac{\hbar \omega}{4}, \\
\end{align*}

and both the mean potential and mean kinetic energy share a value, namely
$\tfrac{\hbar \omega}{4}$.

\prompt{Show that the polarizability of an electron bound by an harmonic force
is tthe same as is found classically, namely $e^2/k$, where $e$ is the electric
charge and $k$ is the spring constant.}

The polarizability is the measure of how rapidly the dipole moment changes when
an external electric potential ($V$) is applied.

\[
  d = \chi V
\]

It is also useful to note that

\[
  k = m \omega^2.
\]

Because we are dealing with single electrons, the dipole moment $d$ is simply
$ex$, where $e$ is the electric charge and $x$ is the displacement.

In order to figure out $\chi$ we will need to determine the Hamiltonian of our
new system.

\[
  H = - \frac{\hbar^2}{2m} \dv[2]{x} + \frac1{2} m \omega^2 x^2 - exV
\]

It is possible to solve this perturbed Hamiltonian using perturbation theory.
However, one can also simply solve it exactly.  We will be solving it exactly.

First, because the applied external potential is essentially a change to the
potential energy, let's consider what happens if we attempt to combine these
terms.  Let's complete the square.

\begin{align*}
  H &= - \frac{\hbar^2}{2m} \dv[2]{x} + \frac1{2} m \omega^2 x^2 - exV \\
  H &= T + U \\
  U &= \frac1{2} m \omega^2 x^2 - exV \\
  ax^2 + bx &= a\qty(x^2 + \frac{b}{a} x) \\
  &= a \qty(x^2 + \frac{b}{a} x + \frac{b^2}{4 a^2} - \frac{b^2}{4 a^2}) \\
  &= a {\qty(x + \frac{b}{2a})}^2 - \frac{b^2}{4 a} \\
  U &= \frac1{2} m \omega^2 {\qty(x - \frac{eV}{m \omega^2})}^2 - \frac{e^2 V^2}{2 m \omega^2}
\end{align*}

If we do a change of variables such that $x' = \qty(x - \tfrac{eV}{m
\omega^2})$.  Then, because 

\begin{align*}
  \dv{x} &= \dv{x'} \dv{x'}{x} \\ 
  \dv{x'}{x} &= 1,
\end{align*}

we have

\begin{align*}
  H &= - \frac{\hbar^2}{2m} \dv[2]{(x')} + \frac1{2} m \omega^2 {(x')}^2 - \frac{e^2 V^2}{2 m \omega^2} \\
  &= H_0 + C.
\end{align*}

Where $H_0$ is our familiar and unperturbed Hamiltonian for a quantum harmonic
oscillator, and $C$ is just a constant.  Because $C$ is a constant, if $x_n$ is
an eigenvector of $H_0$, it is also an eigenvector of $H$, because

\begin{align*}
  (H_0 + C)x_n &= H_0 x_n + C x_n \\
  &= E_n x_n + C x_n \\
  &= (E_n + C) x_n
\end{align*}

So $H$ and $H_0$ share eigenvectors and the eigenvalues are just a constant
shift, meaning they have the same energy states (with a shifted $x$ component)
with different energy values.

So we can simply compare the shift in the $x$ component to figure out the
polarizability.

\begin{align*}
  x' &= x - \frac{eV}{m \omega^2} \\
  \Delta x &= \frac{eV}{m \omega^2} \\
  d &= e \Delta x \\
  &= \frac{e^2 V}{m \omega^2} \\
  d &= \chi V \\
  \chi &= \frac{e^2}{m \omega^2} \\
  &= \frac{e^2}{k}
\end{align*}

Which is the classical value we sought to obtain.
