\prompt{\emph{The Lorentz Group}}

\prompt{a) Evaluate the determinant of the Lorentz matrix. Comments?}

As a reminder, the Lorentz Transformation Matrix is

\[
  \mat{L} = \mqty[
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & \gamma & i \beta \gamma \\ 
    0 & 0 & -i \beta \gamma & \gamma
  ]
\]

The determinant of this is just

\begin{align*}
  \det \mat{L} &= \gamma^2 - (-i^2 \beta^2 \gamma^2) \\
  &= \gamma^2(1 - \beta^2)
\end{align*}

Recall the definition of $\gamma$, which is

\[
  \gamma = \frac1{\sqrt{1 - \beta^2}}.
\]

So the determinant of the matrix is just 1.  Which makes sense, as it should
deform, rather than resize, spacetime.

\prompt{b) Prove that the set of Lorentz transformations in one velocity
direction form a group.}

A group is a collection of things and an operation ($\cdot$ for instance) that
acts on two of those things and returns a third.  Groups must be closed under
this operation, meaning that you cannot produce something outside of the group
by applying the operator.

Other requirements are known as the group axioms, and they're listed all the way
in Chapter 10 of the book.

\begin{enumerate}
  \item The group operation is associative.  That is $a \cdot (b \cdot c) = (a
  \cdot b) \cdot c$.
  \item There exists some identity element $e$ such that $a \cdot e = a$ for all
  $a$.
  \item All $a$ have an inverse $\inv{a}$ such that $a \cdot \inv{a} = e$.
\end{enumerate}

In order to simplify calculations, we will be using the other form of the
Lorentz transformation matrix given in the book

\[
  \mat{L}(\alpha) = \mqty[
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & \cosh \alpha & i \sinh \alpha \\
    0 & 0 & -i \sinh \alpha & \cosh \alpha
  ],
\]

where $\beta = \tanh \alpha$.

The group operation will simply become matrix multiplication. It will be useful
to isolate how two matrices multiply together before attempting to do anything
more complex.

\begin{align*}
  \mat{L}(a) \mat{L}(b) &= \mqty[
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & \cosh a \cosh b + \sinh a \sinh b & i(\cosh a \sinh b + \sinh a \cosh b) \\
    0 & 0 & -i(\cosh a \sinh b + \sinh a \cosh b) & \cosh a \cosh b + \sinh a \sinh b
  ] \\
  &= \mqty[
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & \cosh(a + b) & i\sinh(a + b) \\
    0 & 0 & -i\sinh(a + b) & \cosh(a + b)
  ] \\
  &= \mat{L}(a + b)
\end{align*}

So the matrix multiplication becomes merely addition of the arguments to each of
the hyperbolic trig functions.  Because it reduces to addition in this way, it's
simple to see that it will fulfill the group axioms.  But explicitly
demonstrating them is useful.

First, associativity:

\begin{align*}
  \mat{L}(a) (\mat{L}(b) \mat{L}(c)) &= \mat{L}(a) \mat{L}(b + c) = \mat{L}(a + b + c) \\
  &= \mat{L}(a + b) \mat{L}(c) = (\mat{L}(a) \mat{L}(b)) \mat{L}(c)
\end{align*}

Next, the identity element is the addative identity element, which is where
$\alpha = 0$.  One may easily verify this creates and identity matrix, as one
would expect in this case.

\[
  \mat{L}(a) \mat{L}(0) = \mat{L}(a + 0) = \mat{L}(a)
\]

Finally, the inverse is the same as the addative inverse, ie $\inv{a} = -a$.

\[
  \mat{L}(a) \mat{L}(-a) = \mat{L}(a - a) = \mat{L}(0) = \mat{I}
\]
