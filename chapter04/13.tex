\prompt{\emph{The relativistic law for the sum of two velocities.}  Let frame
$K'$ move with uniform velocity $v$ parallel to $K$ and let $K''$ move with
uniform velocity $v'$ relative to $K'$.  What is the velocity $v''$ of $K''$
relative to $K$?}

In problem \rprob{4}{12}, we proved that the action of composing the Lorentz
transformation matrices was given by simply adding the $\alpha$ values.  If we
define for each of $v$, $v'$, and $v''$ the associated alphas $\alpha$,
$\alpha'$, $\alpha''$, then we have simply that $\alpha + \alpha' = \alpha''$
based on our previous result.

We know that $\alpha = \tanh \beta$ from its definition.  Therefore,

\begin{align*}
  \alpha'' &= \alpha + \alpha' \\
  \atanh \beta'' &= \atanh \beta + \atanh \beta' \\
  \beta'' &= \tanh(\atanh \beta + \atanh \beta') \\
  \frac{v''}{c} &= \tanh(\atanh \frac{v}{c} + \atanh \frac{v'}{c}) \\
  v'' &= c \tanh(\atanh \frac{v}{c} + \atanh \frac{v'}{c}) \\
\end{align*}

It is possible to simplify this farther.  Let's consider the abstract value

\[
  \tanh(\atanh a + \atanh b)
\]

Recall one possible definition of $\tanh$ is

\[
  \tanh x = y = \frac{\E{2x} - 1}{\E{2x} + 1}.
\]

By solving this equation for $x$ in terms of $y$, it is possible to derive a
similar definition for $\inv{\tanh} x$:

\[
  \atanh x = \frac1{2} \log(\frac{1 + x}{1 - x})
\]

So our value can be simplified in this way:

\begin{align*}
  \tanh(\atanh a + \atanh b) &= \tanh(\frac1{2} \log(\frac{1 + a}{1 - a}) + \frac1{2} \log(\frac{1 + b}{1 - b})) \\
  &= \tanh(\frac1{2} \qty(\log(\frac{1 + a}{1 - a}) + \log(\frac{1 + b}{1 - b}))) \\
  &= \tanh(\frac1{2} \log(\frac{1 + a}{1 - a} \frac{1 + b}{1 - b})) \\
  &= \tanh(\frac1{2} \log(\frac{1 + a + b + ab}{1 - a - b + ab})) \\
  &= \tanh(\frac1{2} \log(\frac{1 + a + b + ab}{1 - a - b + ab} \frac{\frac{1}{1 + ab}}{\frac{1}{1 + ab}})) \\
  &= \tanh(\frac1{2} \log(\frac{\frac{1 + a + b + ab}{1 + ab}}{\frac{1 - a - b + ab}{1 + ab}})) \\
  &= \tanh(\frac1{2} \log(\frac{1 + \frac{a + b}{1 + ab}}{1 - \frac{a + b}{1 + ab}})) \\
  &= \tanh(\atanh \frac{a + b}{1 + ab}) \\
  &= \frac{a + b}{1 + ab}
\end{align*}

This implies

\begin{align*}
  v'' &= c \tanh(\atanh \frac{v}{c} + \atanh \frac{v'}{c}) \\
  &= c \frac{\frac{v}{c} + \frac{v'}{c}}{1 + \frac{v}{c} \frac{v'}{c}} \\
  &= \frac{c^2 (v + v')}{c^2 + v v'}
\end{align*}
