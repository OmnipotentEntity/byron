\prompt{Derive the matrix $\mat{A}$ of a general Lorentz transformation corresponding
to relative velocity $v/c = \tanh \alpha$ in a direction given yb the spherical
angles $\phi$, $\theta$ by a similarity transformation, $\inv{\mat{R}} \mat{L} \mat{R}$, where $\mat{R}$
is the extension of the rotation matrix, $\mat{R}(\phi, \theta)$ of Eq. (1.23) to a $4
\times 4$ matrix that acts only on the three space coordinates.  Note that
$\inv{\mat{R}} = \transpose{\mat{R}}$.  Assume that the axes of $K$ and $K'$ remain parallel
during their uniform relative motion.  Check your answer to see whether it
reduces properly in the 3 special cases: $\phi = \phi$, $\theta = 0$; $\phi =
0$, $\theta = \pi/2$; $\alpha = 0$.}

Our $\mat{R}$ matrix will be augmented with a 1 on the final diagonal and zeroes
everywhere else, to get

\[
  \mat{R}(\phi, \theta) = \mqty[
    \cos \phi \cos \theta & \sin \phi \cos \theta & -\sin \theta & 0 \\
    -\sin \phi & \cos \phi & 0 & 0 \\
    \cos \phi \sin \theta & \sin \phi \sin \theta & \cos \theta & 0 \\
    0 & 0 & 0 & 1
  ]
\]

And as a reminder our Lorentz transformation matrix is:

\[
  \mat{L}(\alpha) = \mqty[
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & \cosh \alpha & i \sinh \alpha \\
    0 & 0 & -i \sinh \alpha & \cosh \alpha
  ]
\]

Now we simply must perform the two matrix multiplications. This is
straightforward conceptually, but very involved mathematically, so only the
result will be given.

\begin{align*}
  &\inv{\mat{R}}(\phi, \theta) \mat{L}(\alpha) \mat{R}(\phi, \theta) = \\
  &\resizebox{0.9\hsize}{!}{$
  \mqty[
    1 + (\cosh \alpha - 1) \cos^2 \phi \sin^2 \theta & (\cosh \alpha - 1) \cos \phi \sin \phi \sin^2 \theta & -(\cosh \alpha + 1) \cos \phi  \cos \theta \sin \theta & i \cos \phi \sin \theta \sinh \alpha \\
    (\cosh \alpha - 1) \cos \phi \sin \phi \sin^2 \theta  & 1 + (\cosh \alpha - 1) \sin^2 \phi \sin^2 \theta & -(\cosh \alpha + 1) \sin \phi \cos \theta \sin \theta & i \sin \theta \sin \phi \sinh \alpha \\
    -(\cosh \alpha + 1) \cos \phi \cos \theta \sin \theta & -(\cosh \alpha + 1) \sin \phi \cos \theta \sin \theta & 1 + (\cosh \alpha - 1) \cos^2 \theta & i \cos \theta \sinh \alpha \\
    -i \cos \phi \sin \theta \sinh \alpha & -i \sin \theta \sin \phi \sinh \alpha & -i \cos \theta \sinh \alpha & \cosh \alpha \\
  ]
  $}
\end{align*}

To verify this transformation matrix, the requested special cases will be
tested.

\medskip

$\phi = \phi$, $\theta = 0$

\[
  \inv{\mat{R}}(\phi, 0) \mat{L}(\alpha) \mat{R}(\phi, 0) = \mqty[
    1 & 0 & 0 & 0 \\
    0 & 1 & 0 & 0 \\
    0 & 0 & \cosh \alpha & i \sinh \alpha \\
    0 & 0 & -i \sinh \alpha & \cosh \alpha
  ]
\]

Which is expected, because a rotation in the $xy$-plane will not affect motion
in the $z$ direction.

\medskip

$\phi = 0$, $\theta = \pi/2$

\[
  \inv{\mat{R}}(0, \pi/2) \mat{L}(\alpha) \mat{R}(0, \pi/2) = \mqty[
    \cosh \alpha & 0 & 0 & i \sinh \alpha \\
    0 & 1 & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    -i \sinh \alpha & 0 & 0 & \cosh \alpha
  ]
\]

With this rotation, we are bringing the $z$ axis to the $x$ axis, so the motion
will be along the $x$ axis, which is why the Lorentz transformation operates
along it.

\medskip

$\alpha = 0$

\[
  \inv{\mat{R}}(\phi, \theta) \mat{L}(0) \mat{R}(\phi, \theta) = \mqty[ \imat{4} ]
\]

Finally, $\alpha = 0$ this represents no transformation whatsoever, so it should
not be surprising that we get the identity matrix.
