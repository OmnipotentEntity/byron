\prompt{Show that the general Lorentz transformation worked out in Problem 14
may be expressed in vector notation as follows:}

\[
  \vec{r'} = \vec{r} + \frac{(\vec{v} \vdot \vec{r})\vec{v}}{v^2} (\gamma - 1) - \vec{v} t \gamma, \quad t' = \qty(t - \frac{\vec{v} \vdot \vec{r}}{c^2}) \gamma
\]

\prompt{where $\gamma = 1/\sqrt{1 - \beta^2}$, $\beta = v/c$, and $\vec{v}$ is
the relative velocity of the two frames.  These formulas are a compact,
convenient way to write the Lorentz transformation between systems with parallel
axes moving with arbitrarily directed univorm relative velocity $\vec{v}$.}

Recalling that $\cosh \alpha = \gamma$ and $i \sinh \alpha = i \beta \gamma$.
It is possible to rewrite the matrix in Problem 14 as the (very slightly
simpler):

\begin{align*}
  &\inv{\mat{R}}(\phi, \theta) \mat{L}(\alpha) \mat{R}(\phi, \theta) = \\
  &\resizebox{0.9\hsize}{!}{$
  \mqty[
    1 + (\gamma - 1) \cos^2 \phi \sin^2 \theta & (\gamma - 1) \cos \phi \sin \phi \sin^2 \theta & -(\gamma + 1) \cos \phi  \cos \theta \sin \theta & i \beta \gamma \cos \phi \sin \theta \\
    (\gamma - 1) \cos \phi \sin \phi \sin^2 \theta  & 1 + (\gamma - 1) \sin^2 \phi \sin^2 \theta & -(\gamma + 1) \sin \phi \cos \theta \sin \theta & i \beta \gamma \sin \theta \sin \phi \\
    -(\gamma + 1) \cos \phi \cos \theta \sin \theta & -(\gamma + 1) \sin \phi \cos \theta \sin \theta & 1 + (\gamma - 1) \cos^2 \theta & i \beta \gamma \cos \theta \\
    -i \beta \gamma \cos \phi \sin \theta & -i \beta \gamma \sin \theta \sin \phi & -i \beta \gamma \cos \theta & \gamma \\
  ]
  $}
\end{align*}

If we take this and multiply it to the left of the vector
$\transpose*{\mqty[x,\:y,\:z,\:ict]}$, we get the resulting vector:

\[
  \resizebox{0.9\hsize}{!}{$
    \mqty[x' \\ y' \\ z' \\ ict'] =
    \mqty[%
      x + \cos \phi \sin \theta \qty(-v t \gamma +(\gamma -1) (x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta)) \\
      y + \sin \phi \sin \theta \qty(-v t \gamma +(\gamma -1) (x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta)) \\
      z + \cos \theta \qty(v t \gamma +(\gamma -1) (x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta)) \\
      ict \gamma + i \gamma \beta(x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta)
    ]
  $}
\]

Keep this in your back pocket for later.  Now let's develop what each piece of the goal formula represents.

\begin{align*}
  \vec{r} &= \mqty(x \\ y \\ z) \\
  \vec{v} \vdot \vec{r} &= \mqty(v_x \\ v_y \\ v_z) \vdot \mqty(x \\ y \\ z) \\
  &= v \mqty(\cos \phi \sin \theta \\ \sin \phi \sin \theta \\ \cos \theta) \vdot \mqty(x \\ y \\ z) \\
  &= v \qty(x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) \\
  \vec{v} t \gamma &= vt \gamma \mqty(\cos \phi \sin \theta \\ \sin \phi \sin \theta \\ \cos \theta) \\
\end{align*}

So let's take this one variable at a time.  $x$:

\begin{align*}
  x' = r_x' &= {\qty(\vec{r} + \frac{(\vec{v} \vdot \vec{r})\vec{v}}{v^2} (\gamma - 1) - \vec{v} t \gamma)}_x \\
  &= x + \frac{v (x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) (v \cos \phi \sin \theta)}{v^2} (\gamma - 1) - vt \gamma \cos \phi \sin \theta \\
  &= x + (x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) (\cos \phi \sin \theta) (\gamma - 1) - vt \gamma \cos \phi \sin \theta \\
  &= x + \cos \phi \sin \theta \qty(-v t \gamma +(\gamma -1) (x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta))
\end{align*}

Which is what we wanted to see. $y$:

\begin{align*}
  y' = r_y' &= {\qty(\vec{r} + \frac{(\vec{v} \vdot \vec{r})\vec{v}}{v^2} (\gamma - 1) - \vec{v} t \gamma)}_y \\
  &= y + \frac{v (x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) (v \sin \phi \sin \theta)}{v^2} (\gamma - 1) - vt \gamma \sin \phi \sin \theta \\
  &= y + (x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) (\sin \phi \sin \theta) (\gamma - 1) - vt \gamma \sin \phi \sin \theta \\
  &= y + \cos \phi \sin \theta \qty(-v t \gamma +(\gamma -1) (x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta))
\end{align*}

Again, this is our goal.  $z$:

\begin{align*}
  z' = r_z' &= {\qty(\vec{r} + \frac{(\vec{v} \vdot \vec{r})\vec{v}}{v^2} (\gamma - 1) - \vec{v} t \gamma)}_z \\
  &= z + \frac{v (x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) (v \cos \theta)}{v^2} (\gamma - 1) - vt \gamma \cos \theta \\
  &= z + (x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta) (\cos \theta) (\gamma - 1) - vt \gamma \cos \theta \\
  &= z + \cos \phi \sin \theta \qty(-v t \gamma +(\gamma -1) (x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta))
\end{align*}

Excellent.  And finally $t$:

\begin{align*}
  t' &= \qty(t - \frac{\vec{v} \vdot \vec{r}}{c^2}) \gamma \\
  &= \qty(t - \frac{v \qty(x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta)}{c^2}) \gamma \\
  &= \qty(t - \frac{\beta}{c} v \qty(x \cos \phi \sin \theta + y \sin \phi \sin \theta + z \cos \theta)) \gamma \\
  ict' &= ict \gamma + i \gamma \beta(x \cos \phi \sin \theta  + y \sin \phi \sin \theta + z \cos \theta)
\end{align*}

And we're done.
