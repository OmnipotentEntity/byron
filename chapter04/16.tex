\prompt{a) Find the eigenvalues and eigenvectors of the Lorentz transformation.
Verify that the eigenvectors are null vectors.}

Finding the eigenvalues and eigenvectors of a $4 \times 4$ matrix is a bit of a
slog, especially one that is as complicated as the matrix from \rprob{4}{14}.

Skipping to the end, the eigenvalues are $\{1, 1, \cosh \alpha + \sinh \alpha,
\cosh \alpha - \sinh \alpha\}$, whereas the eigenvectors are:

\[
  \qty{
    \mqty[-\sec \phi \cot \theta \\ 0 \\ 1 \\ 0],
    \mqty[-\tan \phi \\ 1 \\ 0 \\ 0],
    \mqty[i \cos \phi \sin \theta \\ i \sin \phi \sin \theta \\ i \cos \theta \\ 1],
    \mqty[-i \cos \phi \sin \theta \\ -i \sin \phi \sin \theta \\ -i \cos \theta \\ 1]
  }
\]

As might be obvious, only the final two are going to be null vectors, and since
the eigenvalues for the first two are both $1$, even though $\mat{L}$ is an
orthogonal matrix, these first two are allowed to be nonnull. Moreover, the
first two vectors represent the directions orthogonal to the direction of
travel, and are therefore not affected by the Lorentz transformation.

For the final two vectors:

\begin{align*}
  &{(\pm i \cos \phi \sin \theta)}^2 + {(\pm i \sin \phi \sin \theta)}^2 + {(\pm i \cos \theta)}^2 + 1^2 \\
  = &-(\cos^2 \phi \sin^2 \theta + \sin^2 \phi \sin^2 \theta + \cos^2 \theta) + 1 \\
  = &-(\cos^2 \theta + \sin^2 \theta) + 1 \\
  = &-1 + 1 = 0
\end{align*}

Perhaps the meaning of this problem was to just examine the simple $2 \times 2$
Lortetz transformation matrix.

\prompt{b) Evaluate the determinant of the pure Lorentz matrix.}

We should expect the determinant of the Lorentz matrix to be $1$, on account of
it being a unitary transformation.  One must be careful about the form of the
Lorentz matrix used.  If you use the version with $\gamma$ and $\beta$ you might
wind up being lost in the weeds a bit.  Instead choose the version with $\cosh
\alpha$ and $\sinh \alpha$.

\begin{align*}
  \det \mat{L}(\alpha) &=
    \mqty[
      1 & 0 & 0 & 0 \\
      0 & 1 & 0 & 0 \\
      0 & 0 & \cosh \alpha & i \sinh \alpha \\
      0 & 0 & - i \sinh \alpha & \cosh \alpha
    ] \\
  &= \cosh^2 \alpha - \sinh^2 \alpha \\
  &= 1
\end{align*}

\prompt{c) Any other matrix representation of the Lorentz transformation can be
found by performing a similarity transformation on the matrix $\mat{A}$ (Problem
14).  What is the value of the determinant of \emph{any} matrix representation
of the Lorentz transformation?}

Because a similarity tranformation is given by $\mat{A} = \inv{\mat{P}}
\mat{DP}$, and because the determinant is multiplicative, and because the
determinant of an inverse matrix is the inverse determinant of the matrix, we
can very easily see that the determinant of any matrix representation of the
Lorentz transformation has to be $1$, explicitly:

\begin{align*}
  \mat{A} &= \inv{\mat{P}} \mat{L}(\alpha) \mat{P} \\
  \det \mat{A} &= \det \inv{\mat{P}} \det \mat{L}(\alpha) \det \mat{P} \\
  &= z^{-1} \cdot 1 \cdot z \\
  &= 1
\end{align*}
