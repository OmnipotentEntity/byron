\prompt{The \emph{projection} operator $\mat{P}_i$ is defined as follows:}

\[
  \vec{x}' = \mat{P}_i \vec{x} \equiv \vec{x}_i \pip{\vec{x}_i}{\vec{x}}
\]

\prompt{where $\vec{x}_i$ is a unit vector. This operator is called a projection
operator because all $\vec{x}'$ are in the direction of $\vec{x}_i$ and the
length of $\vec{x}'$ equals the component of $\vec{x}$ in the $\vec{x}_i$
direction, namely $\pip{\vec{x}_i}{\vec{x}}$. In the following, let the
$\vec{x}_i$ be a set of orthonormal vectors which span the $n$-dimensional
vector space $V$. \emph{Prove:}}

\prompt{a) $\mat{P}_i$ is idempotent.}

Conceptually, if you project an already projected vector, it won't change it.
Explicitly:

\begin{align*}
  \mat{P}_i^2 \vec{x} &= \mat{P}_i \mat{P}_i \vec{x} \\
  &= \mat{P}_i \vec{x}_i \pip{\vec{x}_i}{\vec{x}} \\
  \mat{P}_i \vec{x}_i &= \vec{x}_i \pip{\vec{x}_i}{\vec{x}_i} \\
  &= \vec{x}_i \\
  \mat{P}_i \vec{x}_i \pip{\vec{x}_i}{\vec{x}} &= \vec{x}_i \pip{\vec{x}_i}{\vec{x}} \\
  &= \mat{P}_i \vec{x} \\
  \mat{P}_i^2 &= \mat{P}_i
\end{align*}

\prompt{b) $\mat{P}_i \mat{P}_j = 0$ for $i \ne j$. Interpret geometrically.}

Conceptually, if you project a vector onto a first vector, then onto a second
vector orthogonal to the first, then you'll wind up with the zero vectors.  This
is the same as finding that the dot product of two orthogonal vectors is $0$.
Explicitly:

\begin{align*}
  \mat{P}_i \mat{P}_j \vec{x} &= \mat{P}_i \vec{x}_j \pip{\vec{x}_j}{\vec{x}} \\
  &= \vec{x}_i \pip{\vec{x}_i}{\vec{x}_j} \pip{\vec{x}_j}{\vec{x}} \\
  &= \vec{x}_i \cdot 0 \cdot \pip{\vec{x}_j}{\vec{x}} \\
  &= \vec{0} \\
  \mat{P}_i \mat{P}_j &= \mat{0}
\end{align*}

\prompt{c) $\mat{P}_i$ has no inverse.}

If $\mat{P}_i$ has no inverse, that implies it is singular, and has a
non-trivial kernel.  So if we can find two different vectors that map to the
zero vector, that is sufficient for showing that $\mat{P}_i$ has no inverse, one
of these vectors can simply be the zero vector.

Consider the vector $\vec{x}_a$ which is perpendicular to $\vec{x}_i$.  This
vector can simply be $c \vec{x}_j$ for $j \ne i$, provided we're dealing in a
space that is greater than 1D, (n.b.\ this operator is simply the identity
operator in 1D,) but can be in general any linear combination of them.

\begin{align*}
  \mat{P}_i \vec{x}_a &= \vec{x}_i \pip{x_i}{x_a} \\
  &= \vec{x}_i \cdot 0 \\
  &= \vec{0}
\end{align*}

So because $\mat{P}_i$ has a non-trivial kernel, it cannot be invertible.

\prompt{d) $\sum_{i=1}^{n} \mat{P}_i = \mat{I}$.}

Consider:

\[
  \sum_i \mat{P}_i \vec{x} = \sum_i \vec{x}_i \pip{\vec{x}_i}{\vec{x}}
\]

Because $\vec{x}_i$ are all orthogonal and they span the vector space $V$ they
form a basis set.  The collection of inner products can be thought of as the
coordinates of the vector $\vec{x}$ in the basis given by the $\vec{x}_i$.
Because performing a change of basis does not change the vector itself, it's
clear that this operation must be the identity operation, because the choice of
$\vec{x}$ is arbitrary (so it cannot be, for instance, merely a non-trivial
linear transform with $\vec{x}$ as an eigenvector with associated eigenvalue of
$1$ in the general case, because the only linear transform for which that is
true in general is the identity).

\prompt{e) $\mat{P}_i$ is Hermitian.}

By Theorem 4.11 in the book, if $\pip{\vec{x}}{\mat{P}_i \vec{x}}$ is real for
all $\vec{x}$, then $\mat{P}_i$ is Hermitian.

\begin{align*}
  \pip{\vec{x}}{\mat{P}_i \vec{x}} &= \pip{\vec{x}}{\vec{x}_i \pip{\vec{x}_i}{\vec{x}}} \\
  &= \pip{\vec{x}}{\vec{x}_i} \pip{\vec{x}_i}{\vec{x}} \\
  &= \pip{\vec{x}}{\vec{x}_i} \conj{\pip{\vec{x}}{\vec{x}_i}} \\
  &= {\| \pip{\vec{x}}{\vec{x}_i} \|}^2
\end{align*}

The final is necessarily real.  So the operator is Hermitian.

\prompt{f) Let $\mat{A}$ be a self-adjoint linear operator defined on an
$n$-dimensional vector space $V$ with $n$-eigenvalues, $\epsilon_i$, and
$n$-eigenvectors, $\vec{x}_i$. Prove that $\mat{A}$ may be written as}

\[
  \mat{A} = \sum_{i=1}^{n} \epsilon_i \mat{P}_i,
\]

\prompt{where $\mat{P}_i$ is as defined above. This is known as the spectral
theorem for self-adjoint linear operators. (The analogous theorem for normal
operators can also be easily proved.) Note the consistency of this result with
parts (d) and (e) of this problem.}

Please note, because $\mat{A}$ is self-adjoint, it does have a set of
orthonormal vectors $\vec{x}_i$, which act as eigenvectors.  See Theorem 4.18.
(Also note that the book emphasizes distinct eigenvalues, this is because
eigenvectors for repeated eigenvalues can be any collection of vectors which
span the space of those repeated eigenvalues.  Because we can choose the
eigenvectors in use, we can choose these to be orthonormal.)

Consider a general vector $\vec{x}$.  Because the linear operator $\mat{A}$ has
$n$ orthonormal eigenvectors, it forms a span of the vector space $V$.
Therefore, the vector $\vec{x}$ can be written as a linear combination of these
eigenvectors, namely:

\[
  \vec{x} = \sum_i \vec{x}_i \pip{\vec{x}_i}{\vec{x}}
\]

Now consider what happens when you apply the transformation $\mat{A}$ to this
vector:

\begin{align*}
  \mat{A} \vec{x} &= \sum_i \mat{A} \vec{x}_i \pip{\vec{x}_i}{\vec{x}} \\
  &= \sum_i \epsilon_i \vec{x}_i \pip{\vec{x}_i}{\vec{x}} \\
  &= \sum_i \epsilon_i \mat{P}_i \vec{x} \\
  \mat{A} &= \sum_i \epsilon_i \mat{P}_i
\end{align*}

Which is what was sought.
