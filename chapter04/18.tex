\prompt{Given any linear transformation $A$ on a finite-dimensional vector
space, prove that in a given coordinate system, the adjoint matrix always
exists.}

\prompt{\emph{Hint}: Modify the reasoning of Theorem 4.7.}

Let the matrix $\mat{A}_X$ be $(a_{ij})$ with respect to a (not-necessarily
orthonormal) basis $X$ that represents the linear transformation $A$.

Using the \emph{Grahm-Schmitt orthogonalization process} (see section 4.3), we
can construct an orthonormal basis $Y$ using $X$.  Let the transformation from
coordinates in $X$ to coordinates in $Y$ be given by $\mat{X}_Y$ with entries
$(x_{ij})$.  So we have that $\mat{A}_Y = \mat{X}_Y \mat{A}_X$ is the
representation of $A$ in the basis $Y$.

The entries of this matrix are:

\[
  {\qty[\mat{X}_Y \mat{A}_X]}_{ij} = \sum_{k} x_{ik} a_{kj} \\
\]

Now following the reasoning from Theorem 4.7:

\begin{align*}
  \sum_{k} x_{ik} a_{kj} &= \pip{y_i}{Ay_j} \\
  &= \pip{\adj{A}y_i}{y_j} \\
  &= \conj{\pip{y_j}{\adj{A}y_i}} \\
  &= \conj{[\adj{A}]}_{ji} \\
  &= \conj{[\adj{\qty(\mat{X}_Y \mat{A}_X)}]}_{ji} \\
  &= \conj{[\adj{\mat{A}}_X \adj{\mat{X}}_Y]}_{ji} \\
  &= \sum_{k} \conj{(a_{jk} a_{ki})}
\end{align*}

Then finally we can transform this matrix back to the $X$ basis by applying the
inverse transformation.

\[
  \adj{\mat{A}}_X = \inv{\mat{X}}_Y \adj{\mat{A}}_Y \adj{\mat{X}}_Y
\]

Because all basis transformations are reversible the inverse matrix has to
exist, and because $Y$ is an orthonormal basis, the adjoint matrices in this
basis exist.  So the linear transformation $A$ has an adjoint, even in
non-orthonormal coordinates.

Theoretically, this matrix should be independent of the choice of the
orthonormal basis $Y$.  However, eliminating it through simplification seems
like it might be a bit tedious and awful, and the problem did not call for
finding an explicit form of the linear transformation, just proving existence.
