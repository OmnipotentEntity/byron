\prompt{In the text we obtained the first-order correction to the eigenfunctions
of a linear operator with a nondegenerate spectrum. Using these methods, obtain
the second-order correction. You may assume that the second-order correction is
orthogonal to the zeroth order state from which you start.}

If you're not already familiar with perturbation methods or similar what this
question is asking.  The method developed was a first-order correction, and that
was of the form

\[
  A = A_0 + \varepsilon A_1.
\]

The second-order correction then is of the form

\begin{equation} \leqn{defOfA}
  A = A_0 + \varepsilon A_1 + \varepsilon^2 A_2.
\end{equation}

This form can be roughly thought of as sort of a Taylor series approximation of
the operator $A$, where the first-order correction is a linear approximation, so
the second-order correction would be the quadratic term in the series.

This derivation will follow the book's derivation of the first-order correction
very closely.

First, we begin from the eigenvalue equation for our known operator

\begin{equation} \leqn{BaseEigenvalueProblem}
  A_0 x_n^{(0)} = \lambda_n^{(0)} x_n^{(0)},
\end{equation}

and we compare it to the sought eigenvalue equation

\begin{equation} \leqn{EigenvalueProblem}
  A x_n = \lambda_n x_n,
\end{equation}

where $\lambda_n$ and $x_n$ have the form

\begin{align*}
  \lambda_n &= \lambda_n^{(0)} + \varepsilon \lambda_n^{(1)} + \cdots = \sum_{i=0}^\infty \varepsilon^i \lambda_n^{(i)} \\
  x_n &= x_n^{(0)} + \varepsilon x_n^{(1)} + \cdots = \sum_{i=0}^\infty \varepsilon^i x_n^{(i)}.
\end{align*}

By substituting the expansions into~\reqn{EigenvalueProblem} and dropping the
subscript $n$ on the eigenvalues and eigenvectors, we obtain

\[
  (A_0 + \varepsilon A_1 + \varepsilon^2 A_2) \sum_{i=0}^\infty \varepsilon^i x^{(i)} = \qty(\sum_{i=0}^\infty \varepsilon^i \lambda^{(i)}) \qty(\sum_{i=0}^\infty \varepsilon^i x^{(i)})
\]

or

\begin{equation} \leqn{SumsEverywhere}
  \sum_{i=0}^\infty \varepsilon^i A_0 x^{(i)} + \sum_{i=0}^\infty \varepsilon^{i+1} A_1 x^{(i)} + \sum_{i=0}^\infty \varepsilon^{i+2} A_2 x^{(i)} = \sum_{i,j=0}^\infty \varepsilon^{i+j} \lambda^{(i)} x^{(j)}
\end{equation}

To solve this, we equate coefficients of like powers of $\varepsilon$ on the
left- and right-hand sides of~\reqn{SumsEverywhere}. The right-hand side is not
in a good form for comparison, but if we reshuffle a little, we get

\[
  \sum_{i=0}^\infty \sum_{j=0}^\infty \varepsilon^{i+j} \lambda^{(i)} x^{(j)} = \sum_{\nu=0}^\infty \sum_{\mu=0}^\nu \varepsilon^{\nu} \lambda^{(\mu)} x^{(\nu - \mu)}.
\]

If we set $x^{(\alpha)} \equiv 0$ when $\alpha \le -1$, then
Eq.~\reqn{SumsEverywhere} may be rewritten as

\[
  \sum_{\nu=0}^\infty \varepsilon^\nu A_0 x^{(\nu)} + \sum_{\nu=0}^\infty \varepsilon^\nu A_1 x^{(\nu - 1)} + \sum_{\nu=0}^\infty \varepsilon^\nu A_2 x^{(\nu - 2)} = \sum_{\nu=0}^\infty \sum_{\mu=0}^\nu \varepsilon^{\nu} \lambda^{(\mu)} x^{(\nu - \mu)}.
\]

Now we equate powers of $\varepsilon^\nu$ to get

\[
  A_0 x^{(\nu)} + A_1 x^{(\nu - 1)} + A_2 x^{(\nu - 2)} = \sum_{\mu=0}^\nu \lambda^{(\mu)} x^{(\nu - \mu)}.
\]

or

\begin{equation} \leqn{EquSetInNu}
  (A_0 - \lambda^{(0)})x^{(\nu)} = \sum_{\mu=1}^\nu \lambda^{(\mu)} x^{(\nu - \mu)} - A_1 x^{(\nu - 1)} - A_2 x^{(\nu - 2)}, \quad \nu = 0, 1, \cdots
\end{equation}

Here we write $(A - \lambda^{(0)} I)$ as $(A - \lambda^{(0)})$ to simplify the
notation.  Thus we have an equation for $x^{(\nu)}$ in terms of $x^{(0)},
x^{(1)}, \ldots, x^{(\nu - 1)}$, the lower-order functions. However, the
right-hand side of the equation for $x^{(\nu)}$ also contains $\lambda^{(\nu)}$
which might seem to involve some difficulty, since $x^{(\nu)}$ cannot be found
until we know $\lambda^{(\nu)}$.  However, if we take the inner product of both
sides of~\reqn{EquSetInNu} with $x^{(0)}$, we get

\[
  \pip{x^{(0)}}{(A_0 - \lambda^{(0)})x^{(\nu)}} = \sum_{\mu=1}^\nu \pip{x^{(0)}}{\lambda^{(\mu)} x^{(\nu - \mu)}} - \pip{x^{(0)}}{A_1 x^{(\nu - 1)}} - \pip{x^{(0)}}{A_2 x^{(\nu - 2)}}
\]

Using Eq.~\reqn{BaseEigenvalueProblem} and the fact that $A_0$ is
Hermitian, we see that the left-hand side vanishes. Explicitly,

\begin{align*}
  \pip{x^{(0)}}{(A_0 - \lambda^{(0)})x^{(\nu)}} &= \pip{x^{(0)}}{A_0 x^{(\nu)}} - \pip{x^{(0)}}{\lambda^{(0)} x^{(\nu)}} \\
  &= \pip{\adj{A_0} x^{(0)}}{x^{(\nu)}} - \lambda^{(0)} \pip{x^{(0)}}{x^{(\nu)}} \\
  &= \pip{A_0 x^{(0)}}{x^{(\nu)}} - \lambda^{(0)} \pip{x^{(0)}}{x^{(\nu)}} \\
  &= \lambda^{(0)} \pip{x^{(0)}}{x^{(\nu)}} - \lambda^{(0)} \pip{x^{(0)}}{x^{(\nu)}} \\
  &= 0
\end{align*}

so

\[
  \lambda^{(\nu)} = \pip{x^{(0)}}{A_1 x^{(\nu - 1)}} + \pip{x^{(0)}}{A_2 x^{(\nu - 2)}} - \sum_{\mu = 1}^{\nu - 1} \lambda^{(\mu)} \pip{x^{(0)}}{x^{(\nu - \mu)}}
\]

for $\nu = 1, 2, \cdots$. (Note that $x^{(0)}$ is taken to be normalized here,
so its inner product with itself is 1, which we can do because it is an
eigenvector.) Hence $\lambda^{(\nu)}$ is determined in terms of the
$\lambda^{(i)}$ and $x^{(i)}$ of orders \textit{lower} than $\nu$.

Thus, in principle, we should be able to solve the hieracrchy of equations
implied by Eq.~\reqn{EquSetInNu}, with the first $(n - 1)$ equations
providing the input for the $n$th equation.  The first few equations of the
sequence are

\begin{subequations} \leqn{EqHierarchy}
  \begin{align}
    (A_0 - \lambda^{(0)}) x^{(0)} &= 0, \nonumber \\
    (A_0 - \lambda^{(0)}) x^{(1)} &= - (A_1 - \lambda^{(1)}) x^{(0)}, \leqn{EqHierFirstCorr} \\
    (A_0 - \lambda^{(0)}) x^{(2)} &= - (A_1 - \lambda^{(1)}) x^{(1)} - (A_2 - \lambda^{(2)}) x^{(0)}, \leqn{EqHierSecondCorr} \\
    (A_0 - \lambda^{(0)}) x^{(3)} &= - (A_1 - \lambda^{(1)}) x^{(2)} - (A_2 - \lambda^{(2)}) x^{(1)} + \lambda^{(3)} x^{(0)}, \leqn{EqHierThirdCorr} \\
    (A_0 - \lambda^{(0)}) x^{(4)} &= - (A_1 - \lambda^{(1)}) x^{(3)} - (A_2 - \lambda^{(2)}) x^{(2)} + \lambda^{(3)} x^{(1)} + \lambda^{(4)} x^{(0)}.
  \end{align}
\end{subequations}

The first equation is just Eq.~\reqn{BaseEigenvalueProblem}, and provides the
starting point for solving all the others, order by order.

All of the Eqs.~\reqn{EqHierarchy} except for the first are of the type $Hx =
h$, discussed in Book Theorem (3.17).  Since $\lambda^{(0)}$ is an eigenvalue of
$A_0$, $\det H = 0$.  Therefore $h$ must lie entirely within the subspace
spanned by the eigenvectors of $H$ belonging to nonzero eigenvalues, that is $h$
must be orthogonal to all the eigenvectors of $H$ belonging to a zero
eigenvalue.  In this case, $h$ must be orthogonal to all eigenvectors of $A_0$
belonging to $\lambda^{(0)}$.

Per the problem statement, let us assume that the spectrum of $A_0$ is
nondegenerate, and imagine that we have determined a particular $x^{(0)}$ from
Eq.~\reqn{BaseEigenvalueProblem}.  If we have only nondegenerate eigenvalues in
mind, then there is only \textit{one} eigenvector to which $h$ must be
orthogonal, so Eq.~\reqn{EqHierFirstCorr} can give a solution for $x^{(1)}$ only
if

\[
  \pip{x^{(0)}}{(A_1 - \lambda^{(1)})x^{(0)}} = 0.
\]

Explicitly, this statement is true because starting from
Eq.~\reqn{EqHierFirstCorr}:

\begin{align*}
  (A_0 - \lambda^{(0)}) x^{(1)} &= - (A_1 - \lambda^{(1)}) x^{(0)} \\
  \pip{x^{(0)}}{(A_0 - \lambda^{(0)}) x^{(1)}} &= - \pip{x^{(0)}}{(A_1 - \lambda^{(1)}) x^{(0)}} \\
  \pip{\adj{(A_0 - \lambda^{(0)})} x^{(0)}}{x^{(1)}} &= - \pip{x^{(0)}}{A_1 x^{(0)}} + \pip{x^{(0)}}{\lambda^{(1)} x^{(0)}} \\
  \pip{0}{x^{(1)}} &= - \pip{x^{(0)}}{A_1 x^{(0)}} + \lambda^{(1)} \pip{x^{(0)}}{x^{(0)}} \\
  0 &= - \pip{x^{(0)}}{A_1 x^{(0)}} + \lambda^{(1)}
\end{align*}

Where we have chosen $x^{(0)}$ to be normalized to 1 and have employed the fact
that $A_n$ is self-adjoint for all $n$.  (So $(x, A_n y) = (A_n x, y)$ and the
eigenvalue $\lambda_0$ of $A_0$ is real.)  We finally have:

\begin{subequations}
\begin{equation}
  \lambda^{(1)} = \pip{x^{(0)}}{A_1 x^{(0)}}
\end{equation}

If we want to apply the same logic to Eq.~\reqn{EqHierSecondCorr}, we
get the following:

\begin{align}
    (A_0 - \lambda^{(0)}) x^{(2)} &= - (A_1 - \lambda^{(1)}) x^{(1)} - (A_2 - \lambda^{(2)}) x^{(0)} \nonumber \\
    \pip{x^{(0)}}{(A_0 - \lambda^{(0)}) x^{(2)}} &= - \pip{x^{(0)}}{(A_1 - \lambda^{(1)}) x^{(1)}} - \pip{x^{(0)}}{(A_2 - \lambda^{(2)}) x^{(0)}} \nonumber \\
    0 &= - \pip{x^{(0)}}{(A_1 - \lambda^{(1)}) x^{(1)}} - \pip{x^{(0)}}{A_2 x^{(0)}} + \pip{x^{(0)}}{\lambda^{(2)} x^{(0)}} \nonumber \\
    \lambda^{(2)} &= \pip{x^{(0)}}{(A_1 - \lambda^{(1)}) x^{(1)}} + \pip{x^{(0)}}{A_2 x^{(0)}}
\end{align}

This gives us $\lambda^{(2)}$ in terms of quantities which are, in principle,
known.  Similarly, Eq.~\reqn{EqHierThirdCorr} has a well-defined solution
provided that

\begin{align*}
  \lambda^{(3)} &= \pip{x^{(0)}}{(A_1 - \lambda^{(1)}) x^{(2)}} + \pip{x^{(0)}}{(A_2 - \lambda^{(2)}) x^{(1)}}
\end{align*}
\end{subequations}
